/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
//const domainAccount = "https://localhost:44374";

export const environment = {
  production: false,
  domainAccount: "http://localhost:1700/api",
  // domainAccount: "https://localhost:44312/api",
  //domainAccount: "http://103.1.239.26:44374/api",
  domainDevice: "https://localhost:53185/api",



  writerChannelsUrl: '/writer/channels',
  readerChannelsUrl: '/reader/channels',
  grafanaHome: 'http://localhost:3001/?orgId=1&kiosk',
  jaegerHome: 'http://localhost:16686/search',
  loraServer: 'http://lora.mainflux.io/#/',
  mqttWsUrl: 'ws://localhost:80/mqtt',
};

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbMenuModule, NbListModule, NbButtonModule, NbInputModule, NbSelectModule, NbTabsetModule, NbCardModule, NbCalendarModule, NbDatepickerModule, NbCheckboxModule, NbLayoutModule, NbAlertModule, NbContextMenuModule, NbIconModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

const ListModule = [
  NbMenuModule,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbTabsetModule,
  NbCardModule,
  NbCheckboxModule,
  NbLayoutModule,
  NbAlertModule,
  NbContextMenuModule,
  NbEvaIconsModule,
  NbIconModule 
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ListModule,
    NbDatepickerModule.forRoot(),
  ],
  exports: [
    ListModule,
    NbDatepickerModule,
  ],
})
export class NebularTemplateModule { }

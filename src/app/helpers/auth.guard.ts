import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";

// import { AuthenticationService } from '../_services';

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router // private authenticationService: AuthenticationService
  ) {}

  canActivate() {
    console.log(localStorage.getItem("token"));

    if (localStorage.getItem("token") != null) {
      return true;
    } else {
      console.log('xlxjx');

       this.router.navigate(["/login"]);
      return false;
    }

    // not logged in so redirect to login page with the return url
    // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    // return false;

    // Check tài khoản đã login chưa
    // Nếu login rồi thì trả ra true
    // Else false  và trỏ vê commponent Login
  }
}

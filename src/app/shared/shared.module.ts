import { NgModule } from '@angular/core';
import { ThemeModule } from '../@theme/theme.module';
import {
  NbCardModule,
  NbIconModule,
  NbButtonModule,
  NbSelectModule,
  NbListModule,
  NbInputModule,
} from '@nebular/theme';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ActionCloumnModule } from './actions-column/actions-column.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CustomActionsColumnComponent } from './custom-actions-column/custom-actions-column.component';
import { CustomActionsColumnModule } from './custom-actions-column/custom-actions-column.module';

@NgModule({
  imports: [
    ThemeModule,
    NbButtonModule,
    NbCardModule,
    NbSelectModule,
    NbListModule,
    NbInputModule,
    Ng2SmartTableModule,
    ActionCloumnModule,
    CustomActionsColumnModule

  ],
  declarations: [
    ConfirmationComponent,
  ],
  exports: [
    ThemeModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbSelectModule,
    NbListModule,
    NbButtonModule,
    Ng2SmartTableModule,
    ActionCloumnModule,
    CustomActionsColumnModule

  ],
})

export class SharedModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomActionsColumnComponent } from './custom-actions-column.component';

describe('CustomActionsColumnComponent', () => {
  let component: CustomActionsColumnComponent;
  let fixture: ComponentFixture<CustomActionsColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomActionsColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomActionsColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

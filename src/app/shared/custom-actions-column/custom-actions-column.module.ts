import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule } from '@angular/forms';
import {
  NbIconModule,
  NbButtonModule,
  NbRadioModule
 } from '@nebular/theme';
import { CustomActionsColumnComponent } from './custom-actions-column.component';
import { CommonModule } from '@angular/common';
@NgModule({
  imports: [
    ThemeModule,
    NbIconModule,
    NbButtonModule,
    NbRadioModule,
    FormsModule
  ],
  declarations: [CustomActionsColumnComponent],

  entryComponents: [
    CustomActionsColumnComponent,
  ],
})
export class CustomActionsColumnModule { }

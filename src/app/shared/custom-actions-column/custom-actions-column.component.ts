import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CostService } from '../../common/services/cost/cost.service';


@Component({
  selector: 'ngx-custom-actions-column',
  templateUrl: './custom-actions-column.component.html',
  styleUrls: ['./custom-actions-column.component.scss']
})
export class CustomActionsColumnComponent implements OnInit {

  checked:boolean = true;
  @Input() rowData: any={};
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() viewdetail: EventEmitter<any> = new EventEmitter();
  @Output() delete: EventEmitter<any> = new EventEmitter();
  @Output() trangpull: EventEmitter<any> = new EventEmitter();
  value: any;
  constructor(
    private router: Router,
    private costService: CostService,
  ) { }
  ngOnInit() {
    console.log(this.rowData);
    this.rowData.IsAccept = 1;

  }
 object = { row: null, event: 0 };
  onEdit() {
    this.edit.emit(this.rowData);
  }

  onDelete() {
    this.delete.emit(this.rowData);
  }

  onViewDetail() {
    this.viewdetail.emit(this.rowData);
  }
  clickAccept(event){
    // object = {}
    console.log(this.rowData.IsAccept);

    this.object.row = this.rowData;
    this.object.event = event;
    this.trangpull.emit(this.object);

  }
}

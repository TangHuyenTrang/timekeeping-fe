import { NgModule } from '@angular/core';
import { ThemeModule } from '../../@theme/theme.module';
import {
  NbIconModule,
  NbButtonModule,
 } from '@nebular/theme';

import { ActionsColumnComponent } from './actions-column.component';

@NgModule({
  imports: [
    ThemeModule,
    NbIconModule,
    NbButtonModule,
  ],
  declarations: [
    ActionsColumnComponent,
  ],
  entryComponents: [
    ActionsColumnComponent,
  ],
})

export class ActionCloumnModule { }

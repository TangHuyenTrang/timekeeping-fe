import { Component, Output, EventEmitter, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-actions-column',
  templateUrl: './actions-column.component.html',
  styleUrls: ['./actions-column.component.scss'],
})
export class ActionsColumnComponent  implements  OnInit {
  // Depends on valuePrepareFunction
  @Input() rowData: any;
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() viewdetail: EventEmitter<any> = new EventEmitter();
  @Output() delete: EventEmitter<any> = new EventEmitter();
  value: any;
  constructor(
    private router: Router,
  ) { }
  ngOnInit() {
  }

  onEdit() {
    this.edit.emit(this.rowData);
  }

  onDelete() {
    this.delete.emit(this.rowData);
  }

  onViewDetail() {
    this.viewdetail.emit(this.rowData);
  }

}

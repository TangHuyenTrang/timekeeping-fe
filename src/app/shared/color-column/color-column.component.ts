import { Component, OnInit, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
  templateUrl: './color-column.component.html',
  styleUrls: ['./color-column.component.scss']
})
export class ColorColumnComponent implements OnInit, ViewCell {
  @Input() rowData: any;
  @Input() value: string | number;
  
  constructor() { }
  ngOnInit() {
  }

}

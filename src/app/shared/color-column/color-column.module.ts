import { ColorColumnComponent } from 'app/shared/color-column/color-column.component';
import { NgModule } from '@angular/core';
import { ThemeModule } from 'app/@theme/theme.module';

@NgModule({
  imports: [
    ThemeModule
  ],
  declarations: [
    ColorColumnComponent,
  ],
  entryComponents: [
    ColorColumnComponent,
  ],
})

export class ColorCloumnModule { }

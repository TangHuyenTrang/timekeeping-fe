import { ExtraOptions, RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import {
  NbAuthComponent,
  NbLoginComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from "@nebular/auth";
import { LoginComponent } from "./auth/login/login.component";
import { AuthGuard } from "./helpers/auth.guard";

const routes: Routes = [
  { path: "", redirectTo: "pages", pathMatch: "full" },
  {
    path: "pages",
    // canActivate: [AuthGuard],s
    loadChildren: () =>
      import("./pages/pages.module").then((m) => m.PagesModule),
  },
  {
    path: "login",
    component: LoginComponent,
  },
     {
      path: 'logout',
      component: NbLogoutComponent,
    },
  // children: [
  //   {
  //     path: '',
  //     component: LoginComponent,
  //   },
  //   {
  //     path: 'login',
  //     component: NbLoginComponent,
  //   },
  //   {
  //     path: 'register',
  //     component: NbRegisterComponent,
  //   },
  //   {
  //     path: 'logout',
  //     component: NbLogoutComponent,
  //   },
  //   {
  //     path: 'request-password',
  //     component: NbRequestPasswordComponent,
  //   },
  //   {
  //     path: 'reset-password',
  //     component: NbResetPasswordComponent,
  //   },
  // ],

  { path: "**", redirectTo: "pages" },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

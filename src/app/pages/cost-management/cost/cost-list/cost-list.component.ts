import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { GroupcostService } from "../../../../common/services/group-cost/groupcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { CostService } from '../../../../common/services/cost/cost.service';

@Component({
  selector: 'ngx-cost-list',
  templateUrl: './cost-list.component.html',
  styleUrls: ['./cost-list.component.scss']
})
export class CostListComponent implements OnInit {
  constructor(
  private dialogService: NbDialogService,
    private router: Router,
    private costService: CostService,
    private notificationsService: NotificationsService
  ) // protected ref: NbDialogRef<ProjectListComponent>,
  {}

  type =  2;//ke toan
  settings = {
    hideSubHeader: true, // hide filter row
    actions: false,
    columns: {
      Title: {
        title: "Nội dung",
        type: "string",
      },
      Note: {
        title: "Ghi chú",
        type: "string",
      },
      Currency: {
        title: "Đơn giá",
        type: "string",
      },
      CreateDate: {
        title: "Ngày lập",
        type: "0",
      },
      GroupName: {
        title: "Nhóm thu chi",
        type: "string",
      },
      ItemName: {
        title: "Khoản mục",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
          instance.viewdetail.subscribe((row) => {
            this.gotoEditRow(row);
          });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();

  getAll() {
    this.source.load([]);
    this.costService.getAll(this.type).subscribe((result: any) => {
      this.source.load(result.Data);
    });
  }



  gotoEditRow(row) {
    this.router.navigate([`/pages/cost-management/cost/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type:"Xác nhận xóa chi phí" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.costService.deleteCost(row.Id).subscribe((res) => {
            if (!res.Success) {
              this.notificationsService.error("Xóa", res.Message);
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa chi phí thành công");
          });
        }
      });
  }

  addProject() {
    this.router.navigate(["/pages/cost-management/cost/add"]);
  }
}

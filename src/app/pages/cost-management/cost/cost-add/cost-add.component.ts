import { RouterModule } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { CostService } from '../../../../common/services/cost/cost.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { GroupcostService } from '../../../../common/services/group-cost/groupcost.service';
import { ItemcostService } from '../../../../common/services/item-cost/itemcost.service';
import { AppService } from '../../../../common/services/AppService';

@Component({
  selector: 'ngx-cost-add',
  templateUrl: './cost-add.component.html',
  styleUrls: ['./cost-add.component.scss']
})
export class CostAddComponent implements OnInit {

  listPerSelect: any = [];
  listPerGroup: any = [];
  costModel = {
    id: 0,
    currency: 0,
    title: '',
    note: '',
    //createDate: '',
    groupCostId: '',
    itemsCostId: '',
    type: 2,//ke toan
    isAccept: 1 //success
  };

  time: any = "";
  lstGroupCost = [];
  lstItemCost = [];
  constructor(private costService: CostService,
    private router: Router,
    private notificationsService: NotificationsService,
    private groupCostService: GroupcostService,
    private itemCostService: ItemcostService,
    private appService: AppService
    ) { }
  ngOnInit() {
    this.groupCostService.getAll().subscribe(
      (result:any) => {
          this.lstGroupCost = result.Data;
      }
   )
   this.itemCostService.getAll().subscribe(
    (result:any) => {
        this.lstItemCost = result.Data;
    })
  }

  addCost(){
    this.costService.add(this.costModel).subscribe(res =>{
      console.log(this.costModel);
      if(!res.Success) {
        this.notificationsService.error('Cập nhật', res.Message);
        return;
      }
      this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
      this.router.navigate(['/pages/cost-management/cost/list']);
  });
}

  backToList() {
    this.router.navigate(['/pages/cost-management/cost/list']);
  }

  // receiverDate(event) {
  //   this.costModel.createDate = this.appService.covertTime(event);
  // }
}


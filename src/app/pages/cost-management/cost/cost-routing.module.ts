import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CostListComponent } from './cost-list/cost-list.component';
import { CostAddComponent } from './cost-add/cost-add.component';
import { CostEditComponent } from './cost-edit/cost-edit.component';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: CostEditComponent,
    },
    {
      path: 'list',
      component: CostListComponent,
    },
    {
      path: 'add',
      component: CostAddComponent,
    }
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CostRoutingModule { }

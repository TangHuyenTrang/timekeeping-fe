import { AppService } from './../../../../common/services/AppService';
import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { CostService } from '../../../../common/services/cost/cost.service';
import { GroupcostService } from '../../../../common/services/group-cost/groupcost.service';
import { ItemcostService } from '../../../../common/services/item-cost/itemcost.service';

@Component({
  selector: 'ngx-cost-edit',
  templateUrl: './cost-edit.component.html',
  styleUrls: ['./cost-edit.component.scss']
})
export class CostEditComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private costService: CostService,
    private router: Router,
    private appService: AppService,
    private groupCostService: GroupcostService,
    private itemCostService: ItemcostService,
    ) {
      this.costModel.id = +this.routerActive.snapshot.paramMap.get("id");

    if (this.costModel.id != 0) {
      this.getCostDetail();
    }
     }

     costModel = {
      id: 0,
      currency: 0,
      title: '',
      note: '',
      type: 0,
      //createDate: '',
      groupCostId: '',
      itemsCostId: '',
     };
     time: any = "";
     lstGroupCost = [];
     lstItemCost = [];
  ngOnInit() {
    this.getCostDetail();
    this.groupCostService.getAll().subscribe(
      (result:any) => {
          this.lstGroupCost = result.Data;
      }
   )
   this.itemCostService.getAll().subscribe(
    (result:any) => {
        this.lstItemCost = result.Data;
    })
  }
  getCostDetail() {
    this.costService
      .getCostById(this.costModel.id)
      .subscribe((result) => {
        this.costModel.currency = result.Data.Currency;
        this.costModel.title = result.Data.Title;
        this.costModel.type = result.Data.Type;
        this.costModel.note = result.Data.Note;
        this.costModel.groupCostId = result.Data.GroupCostId;
        this.costModel.itemsCostId = result.Data.ItemsCostId;
        // this.time = this.appService.formatTimeDisplayNebular(
        //   this.costModel.createDate
        // );
      });
  }

  updateCost() {
    this.costService.updateCost(this.costModel).subscribe((res) => {
      //console.log(this.costModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.router.navigate(["/pages/cost-management/cost/list"]);
    });
  }

  backToList() {
    this.router.navigate(["/pages/cost-management/cost/list"]);
  }
  // receiverDate(event) {
  //   this.costModel.createDate = this.appService.covertTime(event);
  // }
}

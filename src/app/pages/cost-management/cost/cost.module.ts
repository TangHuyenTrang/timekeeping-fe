import { NgModule } from '@angular/core';
import { CostRoutingModule } from './cost-routing.module';
import { CostListComponent } from './cost-list/cost-list.component';
import { CostAddComponent } from './cost-add/cost-add.component';
import { CostEditComponent } from './cost-edit/cost-edit.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

import { CommonModule } from '../../../common/common.module';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { CostService } from '../../../common/services/cost/cost.service';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { GroupcostService } from '../../../common/services/group-cost/groupcost.service';
import { ItemcostService } from '../../../common/services/item-cost/itemcost.service';
import { AppService } from '../../../common/services/AppService';

@NgModule({
  declarations: [CostListComponent, CostAddComponent, CostEditComponent],
  imports: [
    CommonModule,
    CostRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    CostService,
    NotificationsService,
    GroupcostService,
    ItemcostService,
    AppService
  ],
})
export class CostModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CostManagementRoutingModule } from './cost-management-routing.module';

import { SharedModule } from '../../shared/shared.module';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';

import { Ng2SmartTableModule} from 'ng2-smart-table';
import { ConfirmationComponent } from '../../shared/confirmation/confirmation.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CostManagementRoutingModule,
    NbMenuModule,
    SharedModule,
    //CommonModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
  ],
  entryComponents: [
    ConfirmationComponent,
  ],
})
export class CostManagementModule { }

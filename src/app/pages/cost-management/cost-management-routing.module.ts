import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "cost",
        loadChildren: () =>
          import("./cost/cost.module").then((m) => m.CostModule),
      },
      {
        path: "accouting-distribution",
        loadChildren: () =>
          import("./accouting-distribution/accouting-distribution.module").then(
            (m) => m.AccoutingDistributionModule
          ),
      },
      {
        path: "approval-expense",
        loadChildren: () =>
          import("./approval-expense/approval-expense.module").then(
            (m) => m.ApprovalExpenseModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CostManagementRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccoutingDistributionRoutingModule } from './accouting-distribution-routing.module';
import { AccoutingDistribututionListComponent } from './accouting-distributution-list/accouting-distributution-list.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { CostService } from '../../../common/services/cost/cost.service';

import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { GroupcostService } from '../../../common/services/group-cost/groupcost.service';
import { ItemcostService } from '../../../common/services/item-cost/itemcost.service';
import { AppService } from '../../../common/services/AppService';
import { TimekeepingService } from '../../../common/services/timekeeping/timekeeping.service';
import { AccoutingDistribututionEditComponent } from './accouting-distributution-edit/accouting-distributution-edit.component';

@NgModule({
  declarations: [
    AccoutingDistribututionListComponent,
    AccoutingDistribututionEditComponent
  ],
  imports: [
    CommonModule,
    AccoutingDistributionRoutingModule,
    CommonModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    CostService,
    NotificationsService,
    GroupcostService,
    ItemcostService,
    AppService,
    TimekeepingService
  ],
  entryComponents: [AccoutingDistribututionEditComponent],
})
export class AccoutingDistributionModule { }

import { AccoutingDistribututionListComponent } from './accouting-distributution-list/accouting-distributution-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{

  path: '',
  children: [
    // {
    //   path: 'edit/:id',
    //   component: CostEditComponent,
    // },
    {
      path: 'list',
      component: AccoutingDistribututionListComponent,
    },
    // {
    //   path: 'add',
    //   component: CostAddComponent,
    // }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccoutingDistributionRoutingModule { }

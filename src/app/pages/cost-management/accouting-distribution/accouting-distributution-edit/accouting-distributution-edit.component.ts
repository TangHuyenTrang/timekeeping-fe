import { Component, OnInit, Input } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router } from '@angular/router';
import { TaskService } from '../../../../common/services/task/task.service';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { ConfirmationComponent } from '../../../../shared/confirmation/confirmation.component';
import { LocalDataSource } from 'ng2-smart-table';
import { CostService } from '../../../../common/services/cost/cost.service';
import { ItemcostService } from '../../../../common/services/item-cost/itemcost.service';
import { GroupcostService } from '../../../../common/services/group-cost/groupcost.service';

@Component({
  selector: 'ngx-accouting-distributution-edit',
  templateUrl: './accouting-distributution-edit.component.html',
  styleUrls: ['./accouting-distributution-edit.component.scss']
})
export class AccoutingDistribututionEditComponent implements OnInit {

  @Input() idTime: number;
  type = 1;//cham cong
  costModel = {
    id: 0,
    currency: 0,
    title: '',
    note: '',
    createDate: '',
    groupCostId: '',
    itemsCostId: '',
   };
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private notificationsService: NotificationsService,
    protected ref: NbDialogRef<AccoutingDistribututionEditComponent>,
    private costService: CostService,
    private itemCostService: ItemcostService,
    private groupCostService: GroupcostService,
  ) {}

  settings = {
    hideSubHeader: true, // hide filter row
    actions: {
      position: "right",
      delete: false
    },

    edit: {
      editButtonContent: '<i class="ion-edit text-warning" ></i>',
      saveButtonContent: '<i class="ion-checkmark text-primary"></i>',
      cancelButtonContent: '<i class="ion-close text-warning"></i>',
      confirmSave: true,
    },

    // actions: false,
    columns: {
      Note: {
        title: "Ghi chú",
        type: "string",
        editable: false,
      },
      Currency: {
        title: "Chi phí",
        type: "string",
        editable: false,
      },
      ImageUrl: {
        title: "Ảnh minh chứng",
        type: "html",
        valuePrepareFunction: (ImageUrl: string) => {
          return `<img width="250px"  src=\"http://localhost:1700/${ImageUrl}\" />`;
        },
        editable: false,
      },
      GroupCostId: {
        title: "Nhóm chi phí",
        valuePrepareFunction: (cell, row) => {
          return row.GroupName;
        },
        type: "html",
        editor: {
          type: "list",
          config: {
            list: [],
          },
        },
      },
      ItemsCostId: {
        title: "Nhóm khoản mục",
          valuePrepareFunction: (cell, row) => {
          return row.ItemName;
        },
        type: "html",
        editor: {
          type: "list",
          config: {
            list: [],
          },
        },
      },

    },
  };

  ngOnInit() {
    this.getByTime();
    this.getAllGroupCost();
    this.getAllItemCost();
  }

  source: any = new LocalDataSource();

  onSaveConfirm(event) {
    this.dialogService
      .open(ConfirmationComponent, {
        context: { type: "Xác nhận phân nhóm chi phí" },
      })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          console.log('bba', event.newData);
          let id = event.newData.Id;
          let currency = event.newData.Currency;
          let title = event.newData.Title;
          let imageUrl = event.newData.ImageUrl;
          let note = event.newData.Note;
          let groupCostId = event.newData.GroupCostId;
          let itemsCostId = event.newData.ItemsCostId;
          // let date = `${event.newData.nam}-${event.newData.thang}-${event.newData.ngay}`
          let obj = {
            id: id,
            currency: currency,
            title: title,
            imageUrl: imageUrl,
            note: note,
            groupCostId: groupCostId,
            itemsCostId: itemsCostId,
            type: 1,
            timeKeepingId: this.idTime
          };
          this.costService.updateCost(obj).subscribe((result: any) => {
            if (!result.Success) {
              this.notificationsService.error("Cập nhật", result.Message);
              return;
            }
            this.notificationsService.success(
              "Cập nhật",
              "Cập nhật thành công"
            );
            this.router.navigate(["/pages/cost-management/accouting-distribution/list"]);
          });
        }
      });
  }


  dismiss() {
    this.ref.close(false);
  }
  getByTime() {
    this.source.load([]);
    this.costService.getByTimeKeeping(this.idTime).subscribe((result: any) => {
      this.source.load(result.Data);
    });
  }
  getAllGroupCost() {
    this.groupCostService.getAll().subscribe((result: any) => {
      let groupCost = [];
      console.log( result.Data);
      result.Data.forEach((x) => {
        groupCost.push({
          title: x.Name,
          value: x.Id,
        });
      });
      this.settings.columns.GroupCostId.editor.config.list = groupCost;
      this.settings = Object.assign({}, this.settings);
    });
  }

  getAllItemCost() {
    this.itemCostService.getAll().subscribe((result: any) => {
      let itemCost = [];
      result.Data.forEach((x) => {
        itemCost.push({
          title: x.Name,
          value: x.Id,
        });
      });
      this.settings.columns.ItemsCostId.editor.config.list = itemCost;
      this.settings = Object.assign({}, this.settings);
    });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccoutingDistribututionEditComponent } from './accouting-distributution-edit.component';

describe('AccoutingDistribututionEditComponent', () => {
  let component: AccoutingDistribututionEditComponent;
  let fixture: ComponentFixture<AccoutingDistribututionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccoutingDistribututionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccoutingDistribututionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { GroupcostService } from "../../../../common/services/group-cost/groupcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { CostService } from '../../../../common/services/cost/cost.service';
import { TimekeepingService } from '../../../../common/services/timekeeping/timekeeping.service';
import { AccoutingDistribututionEditComponent } from '../accouting-distributution-edit/accouting-distributution-edit.component';

@Component({
  selector: 'ngx-accouting-distributution-list',
  templateUrl: './accouting-distributution-list.component.html',
  styleUrls: ['./accouting-distributution-list.component.scss']
})
export class AccoutingDistribututionListComponent implements OnInit {

  constructor(
    private dialogService: NbDialogService,
      private router: Router,
      private notificationsService: NotificationsService,
      private timekeepingService: TimekeepingService,
    ) // protected ref: NbDialogRef<ProjectListComponent>,
    {}
type = 1;//chamcong
data = [];
settings = {
  hideSubHeader: true, // hide filter row
  actions: false,
  columns: {
    WorkTime: {
      title: "Ngày",
      type: "string",
    },
    Project: {
      title: "Dự án",
      type: "string",
    },
    User: {
      title: "Nhân viên",
      type: "string",
    },

    Cost: {
      title: "Số chi phí",
      type: "string",
    },
  },
};

    ngOnInit() {
      this.getAll();
    }

    source: any = new LocalDataSource();

    getAll() {
      this.source.load([]);
      this.timekeepingService.getAllBySuccess().subscribe((result: any) => {
      //   this.data = result.Data.filter(
      //     x => x.Cost == true
      // );
      this.source.load(result.Data);

      });
    }

    openWithBackdropClick(event) {
      console.log(event);
      this.open(event.data.Id);
    }

    protected open(id: number) {
      this.dialogService.open(AccoutingDistribututionEditComponent, { context: {
        "idTime":id,
      }, closeOnBackdropClick: true });
    }

    gotoEditRow(row) {
      this.router.navigate([`/pages/account-management/project/edit/${row.Id}`]);
    }



}

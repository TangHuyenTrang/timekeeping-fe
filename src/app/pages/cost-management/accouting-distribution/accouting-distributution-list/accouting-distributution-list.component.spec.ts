import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccoutingDistribututionListComponent } from './accouting-distributution-list.component';

describe('AccoutingDistribututionListComponent', () => {
  let component: AccoutingDistribututionListComponent;
  let fixture: ComponentFixture<AccoutingDistribututionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccoutingDistribututionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccoutingDistribututionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApprovalExpenseRoutingModule } from './approval-expense-routing.module';
import { ApprovalExpenseListComponent } from './approval-expense-list/approval-expense-list.component';
import { ApprovalExpenseEditComponent } from './approval-expense-edit/approval-expense-edit.component';


import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { CostService } from '../../../common/services/cost/cost.service';

import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { GroupcostService } from '../../../common/services/group-cost/groupcost.service';
import { ItemcostService } from '../../../common/services/item-cost/itemcost.service';
import { AppService } from '../../../common/services/AppService';
import { TimekeepingService } from '../../../common/services/timekeeping/timekeeping.service';


@NgModule({
  declarations: [ApprovalExpenseListComponent, ApprovalExpenseEditComponent],
  imports: [
    CommonModule,
    ApprovalExpenseRoutingModule,
    CommonModule,
    CommonModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    CostService,
    NotificationsService,
    GroupcostService,
    ItemcostService,
    AppService,
    TimekeepingService
  ],
  entryComponents: [ApprovalExpenseEditComponent],
})
export class ApprovalExpenseModule { }

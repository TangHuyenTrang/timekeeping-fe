import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalExpenseListComponent } from './approval-expense-list.component';

describe('ApprovalExpenseListComponent', () => {
  let component: ApprovalExpenseListComponent;
  let fixture: ComponentFixture<ApprovalExpenseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalExpenseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalExpenseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

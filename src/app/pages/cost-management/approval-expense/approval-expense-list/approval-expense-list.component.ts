import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { GroupcostService } from "../../../../common/services/group-cost/groupcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { CostService } from '../../../../common/services/cost/cost.service';
import { TimekeepingService } from '../../../../common/services/timekeeping/timekeeping.service';
import { ApprovalExpenseEditComponent } from '../approval-expense-edit/approval-expense-edit.component';

@Component({
  selector: 'ngx-approval-expense-list',
  templateUrl: './approval-expense-list.component.html',
  styleUrls: ['./approval-expense-list.component.scss']
})
export class ApprovalExpenseListComponent implements OnInit {


  constructor(
    private dialogService: NbDialogService,
      private router: Router,
      private notificationsService: NotificationsService,
      private timekeepingService: TimekeepingService,
    ) // protected ref: NbDialogRef<ProjectListComponent>,
    {}
type = 1;//chamcong
data = [];
settings = {
  hideSubHeader: true, // hide filter row
  actions: false,
  columns: {
    WorkTime: {
      title: "Ngày",
      type: "string",
    },
    Project: {
      title: "Dự án",
      type: "string",
    },
    User: {
      title: "Nhân viên",
      type: "string",
    },

    Cost: {
      title: "Số chi phí",
      type: "string",
    },
  },
};

    ngOnInit() {
      this.getAllBySuccess();
    }

    source: any = new LocalDataSource();

    getAllBySuccess() {
      this.source.load([]);
      this.timekeepingService.getAllBySuccess().subscribe((result: any) => {
      //   this.data = result.Data.filter(
      //     x => x.Cost == true
      // );
      this.source.load(result.Data);

      });
    }

    openWithBackdropClick(event) {
      console.log(event);
      this.open(event.data.Id);
    }

    protected open(id: number) {
      this.dialogService.open(ApprovalExpenseEditComponent, { context: {
        "idTime":id,
      }, closeOnBackdropClick: true });
    }

    gotoEditRow(row) {
      this.router.navigate([`/pages/account-management/project/edit/${row.Id}`]);
    }
  }

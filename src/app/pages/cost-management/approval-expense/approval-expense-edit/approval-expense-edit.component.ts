import { Component, OnInit, Input } from "@angular/core";
import { NbDialogService, NbDialogRef } from "@nebular/theme";
import { Router } from "@angular/router";
import { TaskService } from "../../../../common/services/task/task.service";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { LocalDataSource } from "ng2-smart-table";
import { CostService } from "../../../../common/services/cost/cost.service";
import { ItemcostService } from "../../../../common/services/item-cost/itemcost.service";
import { GroupcostService } from "../../../../common/services/group-cost/groupcost.service";
import { CustomActionsColumnComponent } from "../../../../shared/custom-actions-column/custom-actions-column.component";

@Component({
  selector: "ngx-approval-expense-edit",
  templateUrl: "./approval-expense-edit.component.html",
  styleUrls: ["./approval-expense-edit.component.scss"],
})
export class ApprovalExpenseEditComponent implements OnInit {
  @Input() idTime: number;
  type = 1; //cham cong
  id: number;
  value: number;
  costModel = {
    id: 0,
    currency: 0,
    title: "",
    note: "",
    createDate: "",
    groupCostId: "",
    itemsCostId: "",
  };
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private notificationsService: NotificationsService,
    protected ref: NbDialogRef<ApprovalExpenseEditComponent>,
    private costService: CostService,
    private itemCostService: ItemcostService,
    private groupCostService: GroupcostService
  ) {}

  settings = {
    hideSubHeader: true, // hide filter row
    actions: {
      position: "right",
      delete: false,
    },

    edit: {
      editButtonContent: '<i class="ion-edit text-warning" ></i>',
      saveButtonContent: '<i class="ion-checkmark text-primary"></i>',
      cancelButtonContent: '<i class="ion-close text-warning"></i>',
      confirmSave: true,
    },
    columns: {
      Note: {
        title: "Ghi chú",
        type: "string",
        editable: false,
      },
      Currency: {
        title: "Chi phí",
        type: "string",
        editable: false,
      },
      ImageUrl: {
        title: "Ảnh minh chứng",
        type: "html",
        valuePrepareFunction: (ImageUrl: string) => {
          return `<img width="250px"  src=\"http://localhost:1700/${ImageUrl}\" />`;
        },
        editable: false,
      },
      GroupName: {
        title: "Nhóm chi phí",
        type: "string",
        editable: false,
      },
      ItemName: {
        title: "Khoản mục",
        type: "string",
        editable: false,
      },
      Status: {
        title: "Duyệt",
        // valuePrepareFunction: (cell, row) => {
        //   console.log("Status", row);

        //   return cell;
        // },
        editor: {
          type: "list",
          config: {
            list: [
              { value: "Success", title: "Success" },
              { value: "Pending", title: "Pending" },
              { value: "Refuse", title: "Refuse" },
            ],
          },
        },
      },
    },
  };

  ngOnInit() {
    this.getByTime();
  }

  source: any = new LocalDataSource();

  onSaveConfirm(event) {
    this.dialogService
      .open(ConfirmationComponent, {
        context: { type: "Xác nhận duyệt chi phí" },
      })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.id = event.newData.Id;
          this.value = event.newData.Status;
          switch (event.newData.Status) {
            case "Success":
              this.value = 1;
              break;

            case "Pending":
              this.value = 2;
              break;

            case "Refuse":
              this.value = 3;
              break;
          }
          console.log('a', this.id, this.value);

          this.costService.accept(this.id, this.value).subscribe((result: any) => {
            console.log('bb', result);
            if (!result.body.Success) {
              this.notificationsService.error("Cập nhật", result.Message);
              return;
            }
            this.notificationsService.success(
              "Cập nhật",
              "Cập nhật thành công"
            );
            this.router.navigate(["/pages/cost-management/approval-expense/list"]);
          });
        }
      });
  }

  dismiss() {
    this.ref.close(false);
  }
  getByTime() {
    this.source.load([]);
    this.costService.getByTimeKeeping(this.idTime).subscribe((result: any) => {
      this.source.load(result.Data);
      console.log("getByTime", result.Data);
    });
  }
}

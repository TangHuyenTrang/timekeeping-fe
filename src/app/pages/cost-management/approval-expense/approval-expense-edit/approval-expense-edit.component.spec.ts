import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalExpenseEditComponent } from './approval-expense-edit.component';

describe('ApprovalExpenseEditComponent', () => {
  let component: ApprovalExpenseEditComponent;
  let fixture: ComponentFixture<ApprovalExpenseEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalExpenseEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalExpenseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

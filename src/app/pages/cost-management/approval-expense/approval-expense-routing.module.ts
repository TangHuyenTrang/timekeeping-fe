import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApprovalExpenseListComponent } from './approval-expense-list/approval-expense-list.component';



const routes: Routes = [{

  path: '',
  children: [
    {
      path: 'list',
      component: ApprovalExpenseListComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApprovalExpenseRoutingModule { }

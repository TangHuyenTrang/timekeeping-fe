import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectTaskRoutingModule } from './project-task-routing.module';
import { ProjectTaskListComponent } from './project-task-list/project-task-list.component';
import { SharedModule } from '../../shared/shared.module';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { ConfirmationComponent } from '../../shared/confirmation/confirmation.component';

import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { NotificationsService } from '../../common/services/notifications/notifications.service';
import { NbMomentDateModule } from '@nebular/moment';
import { FormsModule } from '@angular/forms';
import { ProjectTaskDetailComponent } from './project-task-detail/project-task-detail.component';


@NgModule({
  declarations: [ProjectTaskListComponent, ProjectTaskDetailComponent],
  imports: [
    CommonModule,
    ProjectTaskRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbMomentDateModule,
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    NotificationsService
  ],
  entryComponents: [ConfirmationComponent, ProjectTaskDetailComponent],
})
export class ProjectTaskModule { }

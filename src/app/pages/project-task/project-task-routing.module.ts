import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectTaskListComponent } from './project-task-list/project-task-list.component';


const routes: Routes = [
  {
    path: "list",
    component: ProjectTaskListComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectTaskRoutingModule { }

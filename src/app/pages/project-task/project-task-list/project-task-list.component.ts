import { Component, OnInit, Input } from '@angular/core';
import { TaskService } from '../../../common/services/task/task.service';
import { Subscription } from "rxjs";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { ActionsColumnComponent } from "../../../shared/actions-column/actions-column.component";
import { ConfirmationComponent } from "../../../shared/confirmation/confirmation.component";
import { NbDialogService, NbDialogRef } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { NotificationsService } from "../../../common/services/notifications/notifications.service";
import { ProjectService } from '../../../common/services/project/project.service';
import { ProjectTaskDetailComponent } from '../project-task-detail/project-task-detail.component';
import { AppService } from '../../../common/services/AppService';

@Component({
  selector: 'ngx-project-task-list',
  templateUrl: './project-task-list.component.html',
  styleUrls: ['./project-task-list.component.scss']
})
export class ProjectTaskListComponent implements OnInit {
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private taskService: TaskService,
    private notificationsService: NotificationsService,
    private appService: AppService
  ) // protected ref: NbDialogRef<ProjectListComponent>,
  {}

  settings = {
    hideSubHeader: true, // hide filter row
    actions: false,
    columns: {
      Name: {
        title: "Dự án",
        type: "string",
      },
      StartDate: {
        title: "Ngày bắt đầu",
        type: "0",
      },
      EndDate: {
        title: "Ngày kết thúc",
        type: "string",
      }
      // actions: {
      //   title: "Thao Tác",
      //   type: "custom",
      //   width: "150px",
      //   renderComponent: ActionsColumnComponent,
      //   valuePrepareFunction: (cell, row) => {
      //     return row;
      //   },
      //   onComponentInitFunction: (instance) => {
      //     instance.edit.subscribe((row) => {
      //       this.gotoEditRow(row);
      //     });
      //     instance.delete.subscribe((row) => {
      //       this.gotoDeleteRow(row);
      //     });
      //     instance.viewdetail.subscribe((row) => {
      //       this.gotoEditRow(row);
      //     });
      //   },
      //   editable: false,
      //   addable: false,
      //   filter: false,
      // },
    },
  };

  ngOnInit() {
    this.getProjectByUser();
  }

  source: any = new LocalDataSource();

  getProjectByUser() {
    this.source.load([]);
    this.taskService.getProjectByUser().subscribe((result: any) => {
      this.source.load(result.Data.Result);
    });
  }

  showNoti()
  {
    let date = new Date();
    let dateNow = this.appService.covertTime(date);
    this.taskService.getNoti(dateNow).subscribe((result: any) => {
      let data = result.Data.Result;
      console.log(data);
      if(data != null)
      {
        let title = "TASK: " + data.Description ;
        let mess = "DỰ ÁN: " + data.Name.toLowerCase() + " - HOÀN THÀNH: " + data.SoNgayCong  + "/"+  data.SoNgayDuDinh +  " (ngày công)";
        this.notificationsService.primary(title, mess);
      }
      else
      {
        this.notificationsService.primary('Thông báo', "Bạn không có task nào vào ngày hôm nay!");
      }

    });
  }


  openWithBackdropClick(event) {
    this.open(event.data.Id);
  }

  protected open(id: number) {
    this.dialogService.open(ProjectTaskDetailComponent, { context: {
      "idProject":id,
    }, closeOnBackdropClick: true });
  }
}


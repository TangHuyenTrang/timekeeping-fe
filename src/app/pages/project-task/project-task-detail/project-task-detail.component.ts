import { Component, OnInit, Input } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router } from '@angular/router';
import { TaskService } from '../../../common/services/task/task.service';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-project-task-detail',
  templateUrl: './project-task-detail.component.html',
  styleUrls: ['./project-task-detail.component.scss']
})
export class ProjectTaskDetailComponent implements OnInit {

  @Input() idProject: number;
  source: any = new LocalDataSource();
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private taskService: TaskService,
    private notificationsService: NotificationsService,
    protected ref: NbDialogRef<ProjectTaskDetailComponent>
  ) {}

  settings = {
    hideSubHeader: true, // hide filter row
    // actions: {
    //   add: false,
    //   position: "right",
    // },

    actions: false,
    columns: {
      Description: {
        title: "Task",
        type: "string",
      },
      StartDate: {
        title: "Dự kiến ngày bắt đầu",
        type: "string",
      },
      EndDate: {
        title: "Dự kiến ngày kết thúc",
        type: "0",
      },

    },
  };

  ngOnInit() {
    this.getByIdProject(this.idProject);
  }
  getByIdProject(id: number) {
    this.source.load([]);
    this.taskService.getByUser(id).subscribe((result: any) => {
      this.source.load(result.Data.Result);
    });
  }

  dismiss() {
    this.ref.close(false);
  }

}

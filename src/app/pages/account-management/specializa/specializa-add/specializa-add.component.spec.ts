import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializaAddComponent } from './specializa-add.component';

describe('SpecializaAddComponent', () => {
  let component: SpecializaAddComponent;
  let fixture: ComponentFixture<SpecializaAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecializaAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecializaAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

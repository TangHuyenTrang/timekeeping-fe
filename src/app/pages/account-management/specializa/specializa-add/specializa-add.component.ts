import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { SpecializationService } from "../../../../common/services/specialization/specialization.service";

@Component({
  selector: 'ngx-specializa-add',
  templateUrl: './specializa-add.component.html',
  styleUrls: ['./specializa-add.component.scss']
})
export class SpecializaAddComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private specializaService: SpecializationService,
    private router: Router,
    ) {}

     specialModel = {
      id: "0",
      name: ""
     };

  ngOnInit() {}

  addSpecial() {
    this.specializaService.add(this.specialModel).subscribe((res) => {
      //console.log(this.specialModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }

  backToList() {
    this.router.navigate(["/pages/account-management/specializa/list"]);
  }

}

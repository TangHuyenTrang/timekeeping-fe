import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { SpecializationService } from "../../../../common/services/specialization/specialization.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";

@Component({
  selector: 'ngx-specializa-list',
  templateUrl: './specializa-list.component.html',
  styleUrls: ['./specializa-list.component.scss']
})
export class SpecializaListComponent implements OnInit {

  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private specializaService: SpecializationService,
    private notificationsService: NotificationsService
  ) {}


  settings = {
    hideSubHeader: false, // hide filter row
    actions: false,
    columns: {
      Name: {
        title: "Thông tin",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
          instance.viewdetail.subscribe((row) => {
            this.gotoEditRow(row);
          });
        },
        editable: false,
        addable: false,
        filter: true,
      },
    },
  };
  data = [];

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();
  getData() {
    this.source.load([]);
  }

  getAll() {
    this.source.load([]);
    this.specializaService.getAll().subscribe((result: any) => {
      this.source.load(result.Data);
    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/specializa/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa chuyên môn" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.specializaService.delete(row.Id).subscribe((res) => {
            if (!res.Data) {
              this.notificationsService.error("Xóa", "Chuyên môn đã được gán cho user. Vui lòng update User trước tiên");
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa dịch vụ thành công");
          });
        }
      });
  }

  updateAddNew() {
    this.router.navigate(["/pages/account-management/specializa/add"]);
  }
}

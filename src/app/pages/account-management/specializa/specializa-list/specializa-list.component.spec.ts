import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializaListComponent } from './specializa-list.component';

describe('SpecializaListComponent', () => {
  let component: SpecializaListComponent;
  let fixture: ComponentFixture<SpecializaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecializaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecializaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule } from '@angular/forms';


import { SpecializaRoutingModule } from './specializa-routing.module';
import { SpecializaAddComponent } from './specializa-add/specializa-add.component';
import { SpecializaListComponent } from './specializa-list/specializa-list.component';
import { SpecializaEditComponent } from './specializa-edit/specializa-edit.component';

import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { SpecializationService } from '../../../common/services/specialization/specialization.service'

import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule
} from '@nebular/theme';

@NgModule({
  declarations: [SpecializaAddComponent, SpecializaListComponent, SpecializaEditComponent],
  imports: [
    CommonModule,
    SpecializaRoutingModule,
    CommonModule,
    CommonModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    SpecializationService,
    NotificationsService,
  ],
})
export class SpecializaModule { }

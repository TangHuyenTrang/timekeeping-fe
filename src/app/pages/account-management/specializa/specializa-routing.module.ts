import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecializaAddComponent } from './specializa-add/specializa-add.component';
import { SpecializaEditComponent } from './specializa-edit/specializa-edit.component';
import { SpecializaListComponent } from './specializa-list/specializa-list.component';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: SpecializaEditComponent,
    },
    {
      path: 'list',
      component: SpecializaListComponent,
    },
    {
      path: 'add',
      component: SpecializaAddComponent,
    }
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecializaRoutingModule { }

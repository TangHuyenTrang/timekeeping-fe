import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializaEditComponent } from './specializa-edit.component';

describe('SpecializaEditComponent', () => {
  let component: SpecializaEditComponent;
  let fixture: ComponentFixture<SpecializaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecializaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecializaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

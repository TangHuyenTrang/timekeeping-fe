import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { SpecializationService } from "../../../../common/services/specialization/specialization.service";

@Component({
  selector: 'ngx-specializa-edit',
  templateUrl: './specializa-edit.component.html',
  styleUrls: ['./specializa-edit.component.scss']
})
export class SpecializaEditComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private specializaService: SpecializationService,
    private router: Router,
    ) {
      this.specialModel.id = this.routerActive.snapshot.paramMap.get("id");

    if (this.specialModel.id != "0") {
      this.getAccountDetail();
    }
     }

     specialModel = {
      id: "0",
      name: ""
     };

  ngOnInit() {
    this.getAccountDetail();
  }
  getAccountDetail() {
    this.specializaService
      .getById(this.specialModel.id)
      .subscribe((result) => {
        this.specialModel.name = result.Data.Name;
      });
  }

  updateSpecial() {
    this.specializaService.update(this.specialModel).subscribe((res) => {
      //console.log(this.specialModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }

  backToList() {
    this.router.navigate(["/pages/account-management/specializa/list"]);
  }

}

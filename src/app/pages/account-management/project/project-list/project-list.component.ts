import { Component, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { NbDialogService, NbDialogRef } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ProjectService } from "../../../../common/services/project/project.service";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { TaskListComponent } from "../task-list/task-list.component";

@Component({
  selector: "ngx-project-list",
  templateUrl: "./project-list.component.html",
})
export class ProjectListComponent implements OnInit {
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private projectService: ProjectService,
    private notificationsService: NotificationsService
  ) // protected ref: NbDialogRef<ProjectListComponent>,
  {}

  settings = {
    hideSubHeader: true, // hide filter row
    actions: false,
    columns: {
      Name: {
        title: "Dự án",
        type: "string",
      },
      Description: {
        title: "Mô tả",
        type: "string",
        width: "300px",
      },
      Location: {
        title: "Vị trí",
        type: "string",
      },
      Value: {
        title: "Gía trị",
        type: "0",
      },
      ContactUser: {
        title: "Người liên hệ",
        type: "string",
      },
      MaximumManday: {
        title: "Số ngày tối đa",
        type: "string",
      },
      StartDate: {
        title: "Ngày bắt đầu",
        type: "0",
      },
      EndDate: {
        title: "Ngày kết thúc",
        type: "string",
      },
      Service: {
        title: "Dịch vụ",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.viewdetail.subscribe((row) => {
            this.gotoDetailRow(row);
          });
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });

        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();

  getAll() {
    this.source.load([]);
    this.projectService.getAll().subscribe((result: any) => {
      console.log(result);

      this.source.load(result.Data);
    });
  }
  // openWithBackdropClick(event) {
  //   console.log(event);
  //   this.open(event.data.Id);
  // }

  // protected open(id: number) {
  //   this.dialogService.open(TaskListComponent, { context: {
  //     "idProject":id,
  //   }, closeOnBackdropClick: true });
  // }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/project/edit/${row.Id}`]);
  }

  gotoDetailRow(row) {
    this.router.navigate([`/pages/account-management/project/detail/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa dự án" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.projectService.deleteProject(row.Id).subscribe((res) => {
            console.log("aa", res);

            if (!res.body.Success) {
              this.notificationsService.error("Xóa", res.Message);
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa dự án thành công");
            this.backToList();
          });
        }
      });
  }

  addProject() {
    this.router.navigate(["/pages/account-management/project/add"]);
  }

  backToList(){
    this.router.navigate(['/pages/account-management/project/list']);
  }
}

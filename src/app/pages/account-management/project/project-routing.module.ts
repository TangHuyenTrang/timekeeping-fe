import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectAddComponent } from './project-add/project-add.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: ProjectEditComponent,
    },
    {
      path: 'detail/:id',
      component: ProjectDetailComponent,
    },
    {
      path: 'list',
      component: ProjectListComponent,
    },
    {
      path: 'add',
      component: ProjectAddComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }

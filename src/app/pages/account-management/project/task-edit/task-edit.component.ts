import { ProjectService } from './../../../../common/services/project/project.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { DatePipe } from '@angular/common';
import { NbDateService, NbDialogRef } from '@nebular/theme';
import { TaskService } from '../../../../common/services/task/task.service';
import { AppService } from '../../../../common/services/AppService';
import { AccountService } from '../../../../common/services/account/account.service';
import { TaskDetailComponent } from '../task-detail/task-detail.component';

@Component({
  selector: 'ngx-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss']
})
export class TaskEditComponent implements OnInit {

  @Input() idTask: number;
  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private projectService: ProjectService,
    public datepipe: DatePipe,
    private router: Router,
    protected dateService: NbDateService<Date>,
    private taskService: TaskService,
    private appService:AppService,
    private accountService: AccountService,
    protected ref: NbDialogRef<TaskEditComponent>
    )  { }

  taskModel = {
    id: 0,
    description: '',
    userId: '',
    projectId: 0,
    startDate: '',
    endDate: '',
  };
  startTime: any = "";
  endTime: any = "";
  lstUser = []
  public timeStart:any = ''; // Biến trung gian để hiểện thị time lên time Nebular
  public timeEnd:any = '';
  ngOnInit() {
    this.accountService.getAll().subscribe(
      (result:any) => {
          this.lstUser = result.Data;
      }
   )
   this. getTaskDetail();
  }

  getTaskDetail() {
    this.taskService
      .getById(this.idTask)
      .subscribe((data: any) => {
        let result = data.Data[0];
        console.log(result);
        this.taskModel.description = result.Description;

        this.taskModel.projectId = result.ProjectId;
        this.taskModel.startDate =
          result.StartDate
        this.taskModel.endDate =
          result.EndDate
      });
  }


  editTask(){
    this.taskModel.id = this.idTask;

    this.taskService.update(this.taskModel).subscribe(res =>{
      console.log(33, res);

    if(!res.body.Success) {
      this.notificationsService.error('Cập nhật', res.Message);
      return;
    }
    this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
    //this.router.navigate(['/pages/account-management/project/detail/' + this.idProject]);
    this.ref.close(false);

  });
}
dismiss() {
  this.ref.close(false);
}
receiverStartTime(event){
  this.taskModel.startDate = this.appService.covertTime(event);
  console.log(this.taskModel.startDate);
}

receiverEndTime(event){
  this.taskModel.endDate = this.appService.covertTime(event);
}
backToList(){
  this.router.navigate(['/pages/account-management/project/list']);
}
}


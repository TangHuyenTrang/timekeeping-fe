import { Component, OnInit, Input } from "@angular/core";
import { Subscription } from "rxjs";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { NbDialogService, NbDialogRef } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { TaskService } from "../../../../common/services/task/task.service";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { AccountService } from "../../../../common/services/account/account.service";
import { TaskAddComponent } from "../task-add/task-add.component";

@Component({
  selector: "ngx-task-list",
  templateUrl: "./task-list.component.html",
  styleUrls: ["./task-list.component.scss"],
})
export class TaskListComponent implements OnInit {
  @Input() idProject: number;
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private taskService: TaskService,
    private accountService: AccountService,
    private notificationsService: NotificationsService,
    protected ref: NbDialogRef<TaskListComponent>
  ) {}

  settings = {
    hideSubHeader: true, // hide filter row
    actions: {
      add: true,
      position: "right",
    },
    add: {
      addButtonContent: '<i  class="ion-ios-plus-outline "></i>',
      createButtonContent: '<i class="ion-checkmark text-primary" ></i>',
      cancelButtonContent: '<i class="ion-close text-warning"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="ion-edit text-warning" ></i>',
      saveButtonContent: '<i class="ion-checkmark text-primary"></i>',
      cancelButtonContent: '<i class="ion-close text-warning"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a text-danger"></i>',
      confirmDelete: true,
    },
    // actions: false,
    columns: {
      Description: {
        title: "Công việc",
        type: "string",
      },
      FullName: {
        title: "Nhân viên được giao",
        type: "html",
        editor: {
          type: "list",
          config: {
            list: [],
          },
        },
      },
      StartDate: {
        title: "Dự kiến ngày bắt đầu",
        type: "string",
      },
      EndDate: {
        title: "Dự kiến ngày kết thúc",
        type: "0",
      },
      // actions: {
      //   title: 'Thao Tác',
      //   type: 'custom',
      //   width: '150px',
      //   renderComponent: ActionsColumnComponent,
      //   valuePrepareFunction: (cell, row) => {
      //     return row;
      //   },
      //   onComponentInitFunction: (instance) => {
      //     instance.edit.subscribe(row => {
      //       this.gotoEditRow(row);
      //     });
      //     instance.delete.subscribe(row => {
      //       this.gotoDeleteRow(row);
      //     });
      //     instance.viewdetail.subscribe(row => {
      //       this.gotoEditRow(row);
      //     });
      //   },
      //   editable: false,
      //   addable: false,
      //   filter: false,
      // },
    },
  };

  ngOnInit() {
    this.getByIdProject(this.idProject);
    this.getAllUser();
  }

  source: any = new LocalDataSource();

  onSaveConfirm(event) {
    this.dialogService
      .open(ConfirmationComponent, {
        context: { type: " Xác nhận lưu thay đổi? " },
      })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          let id = event.newData.Id;
          let description = event.newData.Description;
          let userId = event.newData.FullName;
          let projectId = event.newData.ProjectId;
          let startDate = event.newData.StartDate;
          let endDate = event.newData.EndDate;
          // let date = `${event.newData.nam}-${event.newData.thang}-${event.newData.ngay}`
          let obj = {
            id: id,
            description: description,
            userId: userId,
            projectId: projectId,
            startDate: startDate,
            endDate: endDate,
          };
          this.taskService.update(obj).subscribe((result: any) => {
            //console.log(this.accountModel);
            if (!result.Success) {
              this.notificationsService.error("Cập nhật", result.Message);
              return;
            }
            this.notificationsService.success(
              "Cập nhật",
              "Cập nhật thành công"
            );
            this.router.navigate(["/pages/account-management/project/list"]);
          });
        }
      });
  }

  onDeleteConfirm(event) {
    console.log(event);
    this.dialogService
      .open(ConfirmationComponent, { context: { type: `` } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          let id = event.data.Id;
          this.taskService.delete(id).subscribe((res) => {
            if (!res.Success) {
              this.notificationsService.error("Xóa", res.Message);
              return;
            }
            this.notificationsService.success("Xóa", "Xoá thành công");
            this.getByIdProject(this.idProject);
          });
        }
      });
  }
  addTask() {
    this.open(this.idProject);
  }
  protected open(id: number) {
    this.dialogService.open(TaskAddComponent, {
      context: {
        idProject: id,
      },
      closeOnBackdropClick: true,
    }).onClose.subscribe(s=>{
      this.getByIdProject(this.idProject)
    },error=>{
      console.log(error)
    })
  };

  dismiss() {
    this.ref.close(false);
  }
  getAll() {
    this.source.load([]);
    this.taskService.getAll().subscribe((result: any) => {
      this.source.load(result.Data);
    });
  }
  getAllUser() {
    this.accountService.getAll().subscribe((result: any) => {
      let fullNames = [];
      result.Data.forEach((x) => {
        fullNames.push({
          title: x.FullName,
          value: x.Id,
        });
      });
      this.settings.columns.FullName.editor.config.list = fullNames;
      this.settings = Object.assign({}, this.settings);
    });
    console.log("end");
  }
  getByIdProject(id: number) {
    this.source.load([]);
    this.taskService.getTaskById(id).subscribe((result: any) => {
      this.source.load(result.Data);
    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/project/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: row.fullName } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.taskService.delete(row.Id).subscribe((res) => {
            if (!res.Success) {
              this.notificationsService.error("Xóa", res.Message);
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa Task thành công");
          });
        }
      });
  }
}

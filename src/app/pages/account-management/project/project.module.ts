import { ServService } from './../../../common/services/serv/serv.service';
import { AccountService } from './../../../common/services/account/account.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ProjectRoutingModule } from './project-routing.module';
import { ProjectAddComponent } from './project-add/project-add.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';

import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { TaskService } from '../../../common/services/task/task.service';
import { ProjectService } from '../../../common/services/project/project.service';

import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbDialogRef,
  NbProgressBarModule,

} from '@nebular/theme';
 import { NbMomentDateModule } from '@nebular/moment';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskAddComponent } from './task-add/task-add.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { TaskEditComponent } from './task-edit/task-edit.component';
import { TimekeepingDetailComponent } from '../../timekeeping/timekeeping-detail/timekeeping-detail.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TimeKeepingDetailTaskComponent } from './time-keeping-detail-task/time-keeping-detail-task.component';
import { AgmCoreModule } from '@agm/core';


@NgModule({
  declarations:
  [ProjectAddComponent,
   ProjectListComponent,
   ProjectEditComponent,
   TaskListComponent,
   TaskAddComponent,
   ProjectDetailComponent,
   TaskEditComponent,
   TaskDetailComponent,
   TimeKeepingDetailTaskComponent],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
   // NbDatepickerModule,
    NbMomentDateModule,
    NbDateFnsDateModule,
    NbProgressBarModule,
    AgmCoreModule.forRoot({
      //apiKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
      apiKey: 'AIzaSyDL6Y0SB_wpJiQwRXA-gDs3tFOTPY0GVSY',
    }),
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    ProjectService,
    TaskService,
    NotificationsService,
    AccountService,
    ServService
    // NbDialogRef,
  ],
  entryComponents: [TaskListComponent, TaskAddComponent, TaskDetailComponent, TaskEditComponent, TimeKeepingDetailTaskComponent],
})
export class ProjectModule { }

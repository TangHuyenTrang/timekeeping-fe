import { AppService } from './../../../../common/services/AppService';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { ProjectService } from '../../../../common/services/project/project.service';
import { NbDateService, NbDialogService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { TaskService } from '../../../../common/services/task/task.service';
import { TaskAddComponent } from '../task-add/task-add.component';
import { TaskDetailComponent } from '../task-detail/task-detail.component';
import { TaskEditComponent } from '../task-edit/task-edit.component';

@Component({
  selector: 'ngx-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private projectService: ProjectService,
    public datepipe: DatePipe,
    private router: Router,
    protected dateService: NbDateService<Date>,
    private appService:AppService,
    private taskService: TaskService,
    private dialogService: NbDialogService,
    ) {

    this.projectModel.id = +this.routerActive.snapshot.paramMap.get('id');
    if(this.projectModel.id != 0){
      this.getProjectDetail();
    }
  }

listTask = [];

  projectModel = {
    id: 0,
    name: '',
    description: '',
    location: '',
    value: '0',
    contactUser: '',
    maximumManday: '',
    startDate: '',
    endDate: '',
    service: '',
    serviceId: '0',
    kpi: 0,
  };


  public timeStart:any = ''; // Biến trung gian để hiểện thị time lên time Nebular
  public timeEnd:any = '';

  ngOnInit() {
   this.getProjectDetail();
   this.getByIdProject();
  }
  getProjectDetail(){
    this.projectService.getProjectById(this.projectModel.id).subscribe(
      result => {
        let data = result.body;
        if(data.Success)
        {

          this.projectModel.name =  data.Data.Name;
          this.projectModel.description = data.Data.Description;
          this.projectModel.location = data.Data.Location;
          this.projectModel.value = data.Data.Value;
          this.projectModel.contactUser = data.Data.ContactUser;
          this.projectModel.maximumManday = data.Data.MaximumManday;
          this.projectModel.startDate = data.Data.StartDate;
          this.projectModel.endDate = data.Data.EndDate;
          this.projectModel.service = data.Data.Service;
          this.projectModel.kpi = data.Data.Kpi;
          //this.projectModel.serviceId = data.Data.ServiceId;
          this.timeStart = this.appService.formatTimeDisplayNebular(this.projectModel.startDate);
          this.timeEnd = this.appService.formatTimeDisplayNebular(this.projectModel.endDate);
        }
    }
    );

  }


  receiverStartTime(event){
    this.projectModel.startDate = this.appService.covertTime(event);
    console.log(this.projectModel.startDate);
  }

  receiverEndTime(event){
    this.projectModel.endDate = this.appService.covertTime(event);
  }

  updateAccount(){
      this.projectService.updateProject(this.projectModel).subscribe(res =>{
      if(!res.Success) {
        this.notificationsService.error('Cập nhật', res.Message);
        return;
      }
      this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
      this.router.navigate(['/pages/account-management/project/list']);
    });
  }

  backToList(){
    this.router.navigate(['/pages/account-management/project/list']);
  }

  getAll() {
    this.taskService.getAll().subscribe((result: any) => {
    });
  }

  getByIdProject() {
    this.taskService.getTaskById(this.projectModel.id).subscribe(res =>{

      if(res.Success) {
       this.listTask = res.Data;
      }
    });
  }

  openAddTask() {
    this.open(this.projectModel.id);
  }

  protected open(id: number) {
    this.dialogService.open(TaskAddComponent, { context: {
      "idProject":id,
    }, closeOnBackdropClick: true }).onClose.subscribe(s=>{
      this.getByIdProject();
    },error=>{
      console.log(error)
    });
  }

  openDetailTask(id)
  {
    console.log(id);
    this.openDetail(id);
  }
  protected openDetail(id: number) {
    this.dialogService.open(TaskDetailComponent, { context: {
      "idTask": id,
    }, closeOnBackdropClick: true }).onClose.subscribe(s=>{
    //  this.getByIdProject();
    },error=>{
      console.log(error)
    });
  }

  openEditTask(id)
  {
    this.openEdit(id);
  }
  protected openEdit(id: number) {
    this.dialogService.open(TaskEditComponent, { context: {
      "idTask": id,
    }, closeOnBackdropClick: true }).onClose.subscribe(s=>{
      this.getByIdProject();
    },error=>{
      console.log(error)
    });
  }

  get status() {
    if (this.projectModel.kpi <= 25) {
      return 'danger';
    } else if (this.projectModel.kpi <= 50) {
      return 'warning';
    } else if (this.projectModel.kpi <= 75) {
      return 'info';
    } else {
      return 'success';
    }
  }


}

import { ServService } from './../../../../common/services/serv/serv.service';
import { AppService } from './../../../../common/services/AppService';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { ProjectService } from '../../../../common/services/project/project.service';
import { NbDateService } from '@nebular/theme';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-project-add',
  templateUrl: './project-add.component.html',
  styleUrls: ['./project-add.component.scss']
})
export class ProjectAddComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private projectService: ProjectService,
    public datepipe: DatePipe,
    private router: Router,
    private service : ServService,
    protected dateService: NbDateService<Date>,
    private appService:AppService
    )  { }

  projectModel = {
    id: '0',
    name: '',
    description: '',
    location: '',
    value: '0',
    contactUser: '',
    maximumManday: '',
    startDate: '',
    endDate: '',
    //service: '',
    serviceId: '0',
  };

  lstService = [];

  public timeStart:any = ''; // Biến trung gian để hiểện thị time lên time Nebular
  public timeEnd:any = '';
  ngOnInit() {
     this.getAllService();
  }

  addProject(){
    console.log(11111,this.projectModel);
    this.projectService.addProject(this.projectModel).subscribe(res =>{
    if(!res.body.Success) {
      this.notificationsService.error('Cập nhật', res.Message);
      return;
    }
    this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
    this.backToList();
  });
}
getAllService() {
  this.service.getAll().subscribe((result: any) => {
    this.lstService = result.Data;
  });
}

receiverStartTime(event){
  this.projectModel.startDate = this.appService.covertTime(event);
}

receiverEndTime(event){
  this.projectModel.endDate = this.appService.covertTime(event);
}
backToList(){
  this.router.navigate(['/pages/account-management/project/list']);
}
}

import { ProjectService } from './../../../../common/services/project/project.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { DatePipe } from '@angular/common';
import { NbDateService, NbDialogRef, NbDialogService } from '@nebular/theme';
import { TaskService } from '../../../../common/services/task/task.service';
import { AppService } from '../../../../common/services/AppService';
import { AccountService } from '../../../../common/services/account/account.service';
import { TimekeepingDetailComponent } from '../../../timekeeping/timekeeping-detail/timekeeping-detail.component';
import { TimeKeepingDetailTaskComponent } from '../time-keeping-detail-task/time-keeping-detail-task.component';

@Component({
  selector: 'ngx-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit {

  @Input() idTask: number;
  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private projectService: ProjectService,
    public datepipe: DatePipe,
    private router: Router,
    protected dateService: NbDateService<Date>,
    private taskService: TaskService,
    private appService:AppService,
    private accountService: AccountService,
    protected ref: NbDialogRef<TaskDetailComponent>,
    private dialogService: NbDialogService,
    )  { }

  taskModel = {
    id: 0,
    description: '',
    userName: '',
    projectId: 0,
    timeKeepingId: 0,
    startDate: '',
    endDate: '',
  };

  history = [];
  startTime: any = "";
  endTime: any = "";
  lstUser = []
  public timeStart:any = ''; // Biến trung gian để hiểện thị time lên time Nebular
  public timeEnd:any = '';
  ngOnInit() {
       this. getTaskDetail();
       console.log(this.idTask);
  }

  getTaskDetail() {
    this.taskService
      .getById(this.idTask)
      .subscribe((data: any) => {
        let result = data.Data[0];
        console.log(result);
        this.taskModel.description = result.Description;

        this.taskModel.projectId = result.ProjectId;
        this.taskModel.projectId = result.ProjectId;
        this.taskModel.timeKeepingId = result.timeKeepingId;
        this.taskModel.userName =
          result.FullName
        this.taskModel.endDate =
          result.EndDate
      });
  }

  getHistory() {
    this.taskService
      .getHistory(this.idTask)
      .subscribe((result: any) => {
        this.history =  result.Data;
      });
  }


dismiss() {
  this.ref.close(false);
}
receiverStartTime(event){
  this.taskModel.startDate = this.appService.covertTime(event);
}

receiverEndTime(event){
  this.taskModel.endDate = this.appService.covertTime(event);
}
backToList(){
  this.router.navigate(["/pages/timekeeping/list/" + this.idTask]);
}
 showChamCong()
  {
    this.backToList();
    this.dismiss();
      //this.open(this.taskModel.timeKeepingId);

  }
  protected open(id: number) {
    this.dialogService.open(TimeKeepingDetailTaskComponent, { context: {
      "idTimeKeeping": id,
    }, closeOnBackdropClick: true }).onClose.subscribe(s=>{
    },error=>{
      console.log(error)
    });
  }
}

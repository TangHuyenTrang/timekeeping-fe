import { TimekeepingDetailComponent } from './../../../timekeeping/timekeeping-detail/timekeeping-detail.component';
import { NotificationsService } from './../../../../common/services/notifications/notifications.service';
import { Component, OnInit, Input } from '@angular/core';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { Router } from '@angular/router';
import { TimekeepingService } from '../../../../common/services/timekeeping/timekeeping.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-time-keeping-detail-task',
  templateUrl: './time-keeping-detail-task.component.html',
  styleUrls: ['./time-keeping-detail-task.component.scss']
})
export class TimeKeepingDetailTaskComponent implements OnInit {

  @Input() idTimeKeeping: number;

  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private timeKeepingService: TimekeepingService,
    private notificationsService: NotificationsService,
    protected ref: NbDialogRef<TimekeepingDetailComponent>,

  ) { }
  source: any = new LocalDataSource();
  ngOnInit() {
    this.getTimeKeepingDetail();

  }
  role = "";
  accept: boolean = true;
  timeKeepingModel = {
    shiftTime: '',
    workTime: '',
    description: '',
    user: '',
    image: '',
    document: '',
    project: '',
    task: '',
    status: '',
    latitude: 0,
    longitude: 0,
    reason:''
   };

  dismiss() {
    this.ref.close(false);
  }

  getAll() {
    this.source =[];
    this.timeKeepingService.getAll().subscribe((result: any) => {
      this.source = result.Data;
    });
  }
  getTimeKeepingDetail() {
    this.timeKeepingService
      .getTimeKeepingById(this.idTimeKeeping)
      .subscribe((result) => {
        this.role = result.Data.RoleId;
        this.timeKeepingModel.workTime = result.Data.WorkTime;
        this.timeKeepingModel.description = result.Data.Description;
        this.timeKeepingModel.project = result.Data.Project;
        this.timeKeepingModel.task = result.Data.TaskName;
        this.timeKeepingModel.user = result.Data.User;
        this.timeKeepingModel.status = result.Data.Status;
        this.timeKeepingModel.document = "http://localhost:1700/" + result.Data.Document;
        this.timeKeepingModel.image = "http://localhost:1700/" + result.Data.Image;
        this.timeKeepingModel.latitude = +result.Data.Latitude;
        this.timeKeepingModel.longitude = +result.Data.Longitude;
        this.timeKeepingModel.reason = result.Data.Reason;
      });
      this.isNhanVien();
  }
  downloadFile(){
    window.open(this.timeKeepingModel.document);
  }

  chapNhanChamCong()
  {
    if(this.role === "52a2fa18-e9f9-407d-b18b-08d7cca1d864")
    {
      this.notificationsService.warn("Warning", "Bạn không đủ quyền");
    }
    else
    {
      this.timeKeepingService.accept(this.idTimeKeeping, 1, '').subscribe((res) => {
        if (!res.Success) {
          this.notificationsService.error("Cập nhật", res.Message);
          return;
        }
        this.dismiss();
        this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      });
      this.dismiss();
    }

  }

  tuChoiChamCong()
  {
    if(this.role === "52a2fa18-e9f9-407d-b18b-08d7cca1d864")
    {
      this.notificationsService.warn("Warning", "Bạn không đủ quyền");
    }
    else
    {
      this.timeKeepingService.accept(this.idTimeKeeping, 3, this.timeKeepingModel.reason).subscribe((res) => {
        if (!res.Success) {
          this.notificationsService.error("Cập nhật", res.Message);
          return;
        }
        this.dismiss();
        this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      });
    }

  }
  backToList() {
    this.router.navigate(["/pages/timekeeping/list"]);
  }
  isNhanVien()
  {
    if(this.role == "52a2fa18-e9f9-407d-b18b-08d7cca1d864")
    {
      this.accept = false;
    }
  }
}

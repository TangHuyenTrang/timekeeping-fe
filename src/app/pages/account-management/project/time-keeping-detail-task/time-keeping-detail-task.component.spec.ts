import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeKeepingDetailTaskComponent } from './time-keeping-detail-task.component';

describe('TimeKeepingDetailTaskComponent', () => {
  let component: TimeKeepingDetailTaskComponent;
  let fixture: ComponentFixture<TimeKeepingDetailTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeKeepingDetailTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeKeepingDetailTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

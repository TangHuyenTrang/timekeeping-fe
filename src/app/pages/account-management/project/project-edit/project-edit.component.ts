import { AppService } from './../../../../common/services/AppService';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { ProjectService } from '../../../../common/services/project/project.service';
import { NbDateService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { ServService } from '../../../../common/services/serv/serv.service';

@Component({
  selector: 'ngx-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.scss']
})
export class ProjectEditComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private projectService: ProjectService,
    public datepipe: DatePipe,
    private router: Router,
    protected dateService: NbDateService<Date>,
    private appService:AppService,
    private servService: ServService,
    ) {

    this.projectModel.id = +this.routerActive.snapshot.paramMap.get('id');
    if(this.projectModel.id != 0){
      this.getProjectDetail();
    }
  }

  projectModel = {
    id: 0,
    name: '',
    description: '',
    location: '',
    value: '0',
    contactUser: '',
    maximumManday: '',
    startDate: '',
    endDate: '',
    service: '',
    serviceId: '0',
  };

  lstSpecial = [];
  lstRole = [];

  public timeStart:any = ''; // Biến trung gian để hiểện thị time lên time Nebular
  public timeEnd:any = '';
  ngOnInit() {
   this.getProjectDetail();
    this.servService.getAll().subscribe(
      (result: any) => {
        this.lstSpecial = result.Data;
      }
   )
  }

  getProjectDetail(){
    this.projectService.getProjectById(this.projectModel.id).subscribe(
      result => {
        let data = result.body.Data;
        this.projectModel.name =  data.Name;
        this.projectModel.description = data.Description;
        this.projectModel.location = data.Location;
        this.projectModel.value = data.Value;
        this.projectModel.contactUser = data.ContactUser;
        this.projectModel.maximumManday = data.MaximumManday;
        this.projectModel.startDate = data.StartDate;
        this.projectModel.endDate = data.EndDate;
        this.projectModel.service = data.Service;
        this.projectModel.serviceId = data.ServiceId;
        this.timeStart = this.appService.formatTimeDisplayNebular(this.projectModel.startDate);
        this.timeEnd = this.appService.formatTimeDisplayNebular(this.projectModel.endDate);
    }
    );

  }


  receiverStartTime(event){
    this.projectModel.startDate = this.appService.covertTime(event);
  }

  receiverEndTime(event){
    this.projectModel.endDate = this.appService.covertTime(event);
  }

  updateAccount(){
      this.projectService.updateProject(this.projectModel).subscribe(res =>{
      if(!res.body.Success) {
        this.notificationsService.error('Cập nhật', res.Message);
        return;
      }
      this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
      this.router.navigate(['/pages/account-management/project/list']);
    });
  }

  backToList(){
    this.router.navigate(['/pages/account-management/project/list']);
  }

}

import { AppService } from './../../../../common/services/AppService';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { ProjectService } from '../../../../common/services/project/project.service';
import { NbDateService } from '@nebular/theme';
import { DatePipe } from '@angular/common';
import { TaskService } from '../../../../common/services/task/task.service';
import { AccountService } from '../../../../common/services/account/account.service';
import { NbDialogService, NbDialogRef } from "@nebular/theme";

@Component({
  selector: 'ngx-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.scss']
})
export class TaskAddComponent implements OnInit {
  @Input() idProject: number;
  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private projectService: ProjectService,
    public datepipe: DatePipe,
    private router: Router,
    protected dateService: NbDateService<Date>,
    private taskService: TaskService,
    private appService:AppService,
    private accountService: AccountService,
    protected ref: NbDialogRef<TaskAddComponent>
    )  { }

  taskModel = {
    id: 0,
    description: '',
    userId: '',
    projectId: 0,
    startDate: '',
    endDate: '',
  };
  lstUser = []
  public timeStart:any = ''; // Biến trung gian để hiểện thị time lên time Nebular
  public timeEnd:any = '';
  ngOnInit() {
    this.accountService.getAll().subscribe(
      (result:any) => {
          this.lstUser = result.Data;
      }
   )
  }

  addTask(){
    this.taskModel.projectId = this.idProject;
    this.taskService.add(this.taskModel).subscribe(res =>{
    if(!res.Success) {
      this.notificationsService.error('Cập nhật', res.Message);
      return;
    }
    this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
    //this.router.navigate(['/pages/account-management/project/detail/' + this.idProject]);
    this.ref.close(false);

  });
}
dismiss() {
  this.ref.close(false);
}
receiverStartTime(event){
  this.taskModel.startDate = this.appService.covertTime(event);
  console.log(this.taskModel.startDate);
}

receiverEndTime(event){
  this.taskModel.endDate = this.appService.covertTime(event);
}
backToList(){
  this.router.navigate(['/pages/account-management/project/list']);
}
}

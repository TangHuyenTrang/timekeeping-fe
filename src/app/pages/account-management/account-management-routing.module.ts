import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
const routes: Routes = [
  {
    path: '',
    children: [
      // {
      //   path: 'units',
      //   loadChildren: './units/units.module#UnitsModule',
      // },
      // {
      //   path: 'administrative-boundaries',
      //   loadChildren: './administrative-boundaries/administrative-boundaries.module#AdministrativeBoundariesModule',
      // },
      {
        path: 'role',
        loadChildren: () => import('./role/role.module')
        .then(m => m.RoleModule),
      },
      {
        path: 'account',
        loadChildren: () => import('./account/account.module')
        .then(m => m.AccountModule),
      },
      {
        path: 'project',
        loadChildren: () => import('./project/project.module')
        .then(m => m.ProjectModule),
      },
      {
        path: 'group-cost',
        loadChildren: () => import('./group-cost/group-cost.module')
        .then(m => m.GroupCostModule),
      },
      {
        path: 'serv',
        loadChildren: () => import('./serv/serv.module')
        .then(m => m.ServModule),
      },
      {
        path: 'specializa',
        loadChildren: () => import('./specializa/specializa.module')
        .then(m => m.SpecializaModule),
      },
      {
        path: 'item-cost',
        loadChildren: () => import('./item-cost/item-cost.module')
        .then(m => m.ItemCostModule),
      },
      {
        path: 'public-holiday',
        loadChildren: () => import('./public-holiday/public-holiday.module')
        .then(m => m.PublicHolidayModule),
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountManagementRoutingModule {
}

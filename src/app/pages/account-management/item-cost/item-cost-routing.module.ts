import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemCostAddComponent } from './item-cost-add/item-cost-add.component';
import { ItemCostEditComponent } from './item-cost-edit/item-cost-edit.component';
import { ItemCostListComponent } from './item-cost-list/item-cost-list.component';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: ItemCostEditComponent,
    },
    {
      path: 'list',
      component: ItemCostListComponent,
    },
    {
      path: 'add',
      component: ItemCostAddComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemCostRoutingModule { }

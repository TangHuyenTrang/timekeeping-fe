import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { ItemcostService } from "../../../../common/services/item-cost/itemcost.service";

@Component({
  selector: 'ngx-item-cost-edit',
  templateUrl: './item-cost-edit.component.html',
  styleUrls: ['./item-cost-edit.component.scss']
})
export class ItemCostEditComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private itemcostService: ItemcostService,
    private router: Router,
    ) {
      this.itemCostModel.id = this.routerActive.snapshot.paramMap.get("id");

    if (this.itemCostModel.id != "0") {
      this.getAccountDetail();
    }
     }

     itemCostModel = {
      id: "0",
      name: "",
      note: "",
     };

  ngOnInit() {
    this.getAccountDetail();
  }
  getAccountDetail() {
    this.itemcostService
      .getById(this.itemCostModel.id)
      .subscribe((result) => {
        this.itemCostModel.name = result.Data.Name;
        this.itemCostModel.note = result.Data.Note;
      });
  }

  updateItemCost() {
    this.itemcostService.update(this.itemCostModel).subscribe((res) => {
      //console.log(this.itemCostModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }
  backToList() {
    this.router.navigate(["/pages/account-management/item-cost/list"]);
  }
}

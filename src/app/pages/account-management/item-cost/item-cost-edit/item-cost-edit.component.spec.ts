import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCostEditComponent } from './item-cost-edit.component';

describe('ItemCostEditComponent', () => {
  let component: ItemCostEditComponent;
  let fixture: ComponentFixture<ItemCostEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCostEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCostEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

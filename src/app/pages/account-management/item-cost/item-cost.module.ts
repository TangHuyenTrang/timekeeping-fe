import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { SharedModule } from '../../../shared/shared.module';
import { ItemCostRoutingModule } from './item-cost-routing.module';
import { ItemCostAddComponent } from './item-cost-add/item-cost-add.component';
import { ItemCostListComponent } from './item-cost-list/item-cost-list.component';
import { ItemCostEditComponent } from './item-cost-edit/item-cost-edit.component';


import { ItemcostService } from '../../../common/services/item-cost/itemcost.service';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';

import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
  declarations: [ItemCostAddComponent, ItemCostListComponent, ItemCostEditComponent],
  imports: [
    ItemCostRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    ItemcostService,
    NotificationsService,
  ],
})
export class ItemCostModule { }

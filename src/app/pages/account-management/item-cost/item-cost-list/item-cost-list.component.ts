import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { ItemcostService } from "../../../../common/services/item-cost/itemcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";


@Component({
  selector: 'ngx-item-cost-list',
  templateUrl: './item-cost-list.component.html',
  styleUrls: ['./item-cost-list.component.scss']
})
export class ItemCostListComponent implements OnInit {

  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private itemCostService: ItemcostService,
    private notificationsService: NotificationsService
  ) {}


  settings = {
    hideSubHeader: false, // hide filter row
    actions: false,
    columns: {
      Name: {
        title: "Khoản mục",
        type: "string",
      },
      Note: {
        title: "Chi tiết",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
          // instance.viewdetail.subscribe((row) => {
          //   this.gotoEditRow(row);
          // });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };
  data = [];

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();
  getData() {
    this.source.load([]);
    // this.accountService.getAccount(this.searchModel.hoten, this.searchModel.email,
    //   this.searchModel.unitId, this.searchModel.roleId).subscribe(
    //     result => {
    //       this.data = result.data.results;
    //       // this.source.load(result.data.results);
    //      // this.mergeData();
    //       this.source.load(this.data);
    //     }
    //   )
  }

  getAll() {
    this.source.load([]);
    this.itemCostService.getAll().subscribe((result: any) => {
      console.log(result.Data);
      this.source.load(result.Data);
    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/item-cost/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa khoản mục" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.itemCostService.delete(row.Id).subscribe((res) => {
            if (!res.Data) {
              this.notificationsService.error("Xóa", "Khoản mục đã được gán cho chi phí. Vui lòng xóa chi phí trước!");
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa khoản mục thành công");
          });
        }
      });
  }

  updateAddNew() {
    this.router.navigate(["/pages/account-management/item-cost/add"]);
  }
}

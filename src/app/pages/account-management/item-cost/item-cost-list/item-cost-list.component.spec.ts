import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCostListComponent } from './item-cost-list.component';

describe('ItemCostListComponent', () => {
  let component: ItemCostListComponent;
  let fixture: ComponentFixture<ItemCostListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCostListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCostAddComponent } from './item-cost-add.component';

describe('ItemCostAddComponent', () => {
  let component: ItemCostAddComponent;
  let fixture: ComponentFixture<ItemCostAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCostAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCostAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { ItemcostService } from "../../../../common/services/item-cost/itemcost.service";

@Component({
  selector: 'ngx-item-cost-add',
  templateUrl: './item-cost-add.component.html',
  styleUrls: ['./item-cost-add.component.scss']
})
export class ItemCostAddComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private itemcostService: ItemcostService,
    private router: Router,
    ) {}

     itemCostModel = {
      id: "0",
      name: "",
      note: "",
     };

  ngOnInit() {}

  addItemCost() {
    this.itemcostService.add(this.itemCostModel).subscribe((res) => {
      //console.log(this.itemCostModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }
  backToList() {
    this.router.navigate(["/pages/account-management/item-cost/list"]);
  }

}

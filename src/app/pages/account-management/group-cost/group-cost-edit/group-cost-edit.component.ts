import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { GroupcostService } from "../../../../common/services/group-cost/groupcost.service";

@Component({
  selector: 'ngx-group-cost-edit',
  templateUrl: './group-cost-edit.component.html',
  styleUrls: ['./group-cost-edit.component.scss']
})
export class GroupCostEditComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private accountService: GroupcostService,
    private router: Router,
    ) {
      this.groupCostModel.id = this.routerActive.snapshot.paramMap.get("id");

    if (this.groupCostModel.id != "0") {
      this.getAccountDetail();
    }
     }

     groupCostModel = {
      id: "0",
      name: "",
      description: "",
     };

  ngOnInit() {
    this.getAccountDetail();
  }
  getAccountDetail() {
    this.accountService
      .getGroupCostById(this.groupCostModel.id)
      .subscribe((result) => {
        this.groupCostModel.name = result.Data.Name;
        this.groupCostModel.description = result.Data.Description;
      });
  }

  updateAccount() {
    this.accountService.update(this.groupCostModel).subscribe((res) => {
      //console.log(this.groupCostModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.router.navigate(["/pages/account-management/group-cost/list"]);
    });
  }

  backToList() {
    this.router.navigate(["/pages/account-management/group-cost/list"]);
  }
}

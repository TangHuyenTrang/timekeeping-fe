import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCostEditComponent } from './group-cost-edit.component';

describe('GroupCostEditComponent', () => {
  let component: GroupCostEditComponent;
  let fixture: ComponentFixture<GroupCostEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCostEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCostEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { SharedModule } from '../../../shared/shared.module';
import { GroupCostRoutingModule } from './group-cost-routing.module';
import { GroupCostAddComponent } from './group-cost-add/group-cost-add.component';
import { GroupCostEditComponent } from './group-cost-edit/group-cost-edit.component';
import { GroupCostListComponent } from './group-cost-list/group-cost-list.component';

import { GroupcostService } from '../../../common/services/group-cost/groupcost.service';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';

import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
  declarations: [
     GroupCostAddComponent,
     GroupCostEditComponent,
     GroupCostListComponent],
  imports: [
    CommonModule,
    GroupCostRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    GroupcostService,
    NotificationsService,
  ],
})
export class GroupCostModule { }

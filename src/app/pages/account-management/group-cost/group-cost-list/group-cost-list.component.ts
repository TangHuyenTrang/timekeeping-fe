import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { GroupcostService } from "../../../../common/services/group-cost/groupcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";

@Component({
  selector: "ngx-group-cost-list",
  templateUrl: "./group-cost-list.component.html",
  styleUrls: ["./group-cost-list.component.scss"],
})
export class GroupCostListComponent implements OnInit {
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private groupCostService: GroupcostService,
    private notificationsService: NotificationsService
  ) {}


  settings = {
    hideSubHeader: false, // hide filter row
    actions: false,
    columns: {
      Name: {
        title: "Nhóm chi phí",
        type: "string",
      },
      Description: {
        title: "Mô tả",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
          // instance.viewdetail.subscribe((row) => {
          //   this.gotoEditRow(row);
          // });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };
  data = [];

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();
  getData() {
    this.source.load([]);
    // this.groupCostService.getAccount(this.searchModel.hoten, this.searchModel.email,
    //   this.searchModel.unitId, this.searchModel.roleId).subscribe(
    //     result => {
    //       this.data = result.data.results;
    //       // this.source.load(result.data.results);
    //      // this.mergeData();
    //       this.source.load(this.data);
    //     }
    //   )
  }

  getAll() {
    this.source.load([]);
    this.groupCostService.getAll().subscribe((result: any) => {
      console.log(result.Data);
      this.source.load(result.Data);
    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/group-cost/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa nhóm chi phí" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.groupCostService.deleteGroupCost(row.Id).subscribe((res) => {
            if (!res.Data) {
              this.notificationsService.error("Xóa", "Khoản mục đã được gán cho chi phí. Vui lòng xóa chi phí trước!");
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa nhóm chi phí thành công");
          });
        }
      });
  }

  updateAddNew() {
    this.router.navigate(["/pages/account-management/group-cost/add"]);
  }
}

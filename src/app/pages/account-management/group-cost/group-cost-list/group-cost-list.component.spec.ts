import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCostListComponent } from './group-cost-list.component';

describe('GroupCostListComponent', () => {
  let component: GroupCostListComponent;
  let fixture: ComponentFixture<GroupCostListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCostListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCostAddComponent } from './group-cost-add.component';

describe('GroupCostAddComponent', () => {
  let component: GroupCostAddComponent;
  let fixture: ComponentFixture<GroupCostAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCostAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCostAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

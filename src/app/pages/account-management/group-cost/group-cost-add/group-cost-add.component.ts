import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { GroupcostService } from "../../../../common/services/group-cost/groupcost.service";

@Component({
  selector: 'ngx-group-cost-add',
  templateUrl: './group-cost-add.component.html',
  styleUrls: ['./group-cost-add.component.scss']
})
export class GroupCostAddComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private accountService: GroupcostService,
    private router: Router,
    ) {}

     groupCostModel = {
      id: "0",
      name: "",
      description: "",
     };

  ngOnInit() {
  }


  addGroupCost() {
    this.accountService.add(this.groupCostModel).subscribe((res) => {
      //console.log(this.groupCostModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
     this.backToList();
    });
  }

  backToList() {
    this.router.navigate(["/pages/account-management/group-cost/list"]);
  }

}

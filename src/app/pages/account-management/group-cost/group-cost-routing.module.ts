import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupCostAddComponent } from './group-cost-add/group-cost-add.component';
import { GroupCostEditComponent } from './group-cost-edit/group-cost-edit.component';
import { GroupCostListComponent } from './group-cost-list/group-cost-list.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: GroupCostEditComponent,
    },
    {
      path: 'list',
      component: GroupCostListComponent,
    },
    {
      path: 'add',
      component: GroupCostAddComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupCostRoutingModule { }

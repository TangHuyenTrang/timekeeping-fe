import { NgModule } from '@angular/core';

import { AccountManagementRoutingModule } from './account-management-routing.module';

import { SharedModule } from '../../shared/shared.module';
//import { CommonModule } from '../../common/common.module';
// import { ThemeModule } from 'app/@theme/theme.module';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';

import { Ng2SmartTableModule} from 'ng2-smart-table';
import { ConfirmationComponent } from '../../shared/confirmation/confirmation.component';
// import { GroupCostComponent } from './group-cost/group-cost.component';
// import { ServComponent } from './serv/serv.component';
// import { SpecializationComponent } from './specialization/specialization.component';
// import { ItemCostComponent } from './item-cost/item-cost.component';
// import { CostComponent } from './cost/cost.component';
//import { ProjectAddComponent } from './project-add/project-add.component';



@NgModule({
  imports: [
    AccountManagementRoutingModule,
    //ThemeModule,
    NbMenuModule,
    SharedModule,
    //CommonModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
  ],
  // declarations: [
  // ProjectAddComponent],
  providers: [
    NbDialogService,
    NbWindowService,
  ],
  entryComponents: [
    ConfirmationComponent,
  ],

})
export class AccountManagementModule {
}

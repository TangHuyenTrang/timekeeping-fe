import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServEditComponent } from './serv-edit.component';

describe('ServEditComponent', () => {
  let component: ServEditComponent;
  let fixture: ComponentFixture<ServEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

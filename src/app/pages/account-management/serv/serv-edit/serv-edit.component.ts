import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { ServService } from "../../../../common/services/serv/serv.service";

@Component({
  selector: 'ngx-serv-edit',
  templateUrl: './serv-edit.component.html',
  styleUrls: ['./serv-edit.component.scss']
})
export class ServEditComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private servService: ServService,
    private router: Router,
    ) {
      this.servModel.id = this.routerActive.snapshot.paramMap.get("id");

    if (this.servModel.id != "0") {
      this.getServDetail();
    }
     }

     servModel = {
      id: "0",
      name: "",
      description: "",
      introDocument: "",
      code: "",
     };

  ngOnInit() {
    this.getServDetail();
  }
  getServDetail() {
    this.servService
      .getServById(this.servModel.id)
      .subscribe((result) => {
        this.servModel.name = result.Data.Name;
        this.servModel.description = result.Data.Description;
        this.servModel.introDocument = result.Data.IntroDocument;
        this.servModel.code = result.Data.Code;
      });
  }

  updateServ() {
    this.servService.update(this.servModel).subscribe((res) => {
      //console.log(this.servModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }

  backToList() {
    this.router.navigate(["/pages/account-management/serv/list"]);
  }

}

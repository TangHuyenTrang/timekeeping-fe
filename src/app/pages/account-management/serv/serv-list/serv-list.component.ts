import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { ServService } from "../../../../common/services/serv/serv.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";


@Component({
  selector: 'ngx-serv-list',
  templateUrl: './serv-list.component.html',
  styleUrls: ['./serv-list.component.scss']
})
export class ServListComponent implements OnInit {

  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private servService: ServService,
    private notificationsService: NotificationsService
  ) {}


  settings = {
    hideSubHeader: true, // hide filter row
    actions: false,
    columns: {
      Name: {
        title: "Dịch vụ",
        type: "string",
      },
      Description: {
        title: "Mô tả",
        type: "string",
      },
      Code: {
        title: "Mã dịch vụ",
        type: "string",
      },
      IntroDocument: {
        title: "Bản giới thiệu dịch vụ",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
          // instance.viewdetail.subscribe((row) => {
          //   this.gotoEditRow(row);
          // });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };
  data = [];

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();
  getData() {
    this.source.load([]);
    // this.accountService.getAccount(this.searchModel.hoten, this.searchModel.email,
    //   this.searchModel.unitId, this.searchModel.roleId).subscribe(
    //     result => {
    //       this.data = result.data.results;
    //       // this.source.load(result.data.results);
    //      // this.mergeData();
    //       this.source.load(this.data);
    //     }
    //   )
  }

  getAll() {
    this.source.load([]);
    this.servService.getAll().subscribe((result: any) => {
      this.source.load(result.Data);
    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/serv/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa dịch vụ" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.servService.deleteServ(row.Id).subscribe((res) => {
            console.log(res);

            if (res.Success != true || res.Data == false) {
              this.notificationsService.error("Xóa", "Không thể xóa vì đã dùng trong dự án. Hãy xóa dự án trước");
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa dịch vụ thành công");
          });
        }
      });
  }

  updateAddNew() {
    this.router.navigate(["/pages/account-management/serv/add"]);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule } from '@angular/forms';

import { ServRoutingModule } from './serv-routing.module';
import { ServAddComponent } from './serv-add/serv-add.component';
import { ServEditComponent } from './serv-edit/serv-edit.component';
import { ServListComponent } from './serv-list/serv-list.component';

import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { ServService } from '../../../common/services/serv/serv.service';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule
} from '@nebular/theme';

@NgModule({
  declarations: [ServAddComponent, ServEditComponent, ServListComponent],
  imports: [
    CommonModule,
    ServRoutingModule,
    CommonModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    ServService,
    NotificationsService,
  ],
})
export class ServModule { }

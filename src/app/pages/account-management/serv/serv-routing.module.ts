import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServAddComponent } from './serv-add/serv-add.component';
import { ServEditComponent } from './serv-edit/serv-edit.component';
import { ServListComponent } from './serv-list/serv-list.component';

const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: ServEditComponent,
    },
    {
      path: 'list',
      component: ServListComponent,
    },
    {
      path: 'add',
      component: ServAddComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServRoutingModule { }

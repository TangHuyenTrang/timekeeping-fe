import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { ServService } from "../../../../common/services/serv/serv.service";
@Component({
  selector: 'ngx-serv-add',
  templateUrl: './serv-add.component.html',
  styleUrls: ['./serv-add.component.scss']
})
export class ServAddComponent implements OnInit {

  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private servService: ServService,
    private router: Router,
    ) {
     }

     servModel = {
      id: "0",
      name: "",
      description: "",
      introDocument: "",
      code: "",
     };

  ngOnInit() {
  }
  addService() {
    this.servService.add(this.servModel).subscribe((res) => {
      //console.log(this.servModel);
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }

  backToList() {
    this.router.navigate(["/pages/account-management/serv/list"]);
  }


}

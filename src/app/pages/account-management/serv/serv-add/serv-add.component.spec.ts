import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServAddComponent } from './serv-add.component';

describe('ServAddComponent', () => {
  let component: ServAddComponent;
  let fixture: ComponentFixture<ServAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicHolidayAddComponent } from './public-holiday-add.component';

describe('PublicHolidayAddComponent', () => {
  let component: PublicHolidayAddComponent;
  let fixture: ComponentFixture<PublicHolidayAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicHolidayAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicHolidayAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

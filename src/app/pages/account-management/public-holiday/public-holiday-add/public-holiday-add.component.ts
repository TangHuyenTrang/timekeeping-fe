import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService, NbDateService } from "@nebular/theme";
import { NavigationEnd, Router, ActivatedRoute } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { ItemcostService } from "../../../../common/services/item-cost/itemcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { PublicHolidayService } from '../../../../common/services/public-holiday/public-holiday.service';
import { AppService } from '../../../../common/services/AppService';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-public-holiday-add',
  templateUrl: './public-holiday-add.component.html',
  styleUrls: ['./public-holiday-add.component.scss']
})
export class PublicHolidayAddComponent implements OnInit {
  constructor(
    private holidayService : PublicHolidayService,
    private notificationsService: NotificationsService,
    private router: Router,
    private appService: AppService,
    public datepipe: DatePipe,
    protected dateService: NbDateService<Date>,
    ) {}
    public timeStart:any = '';
     holidayModel = {
      id: 0,
      holiday: '',
      description: "",
      status: "",
     };

  ngOnInit() {}

  addHoliday() {
    this.holidayService.add(this.holidayModel).subscribe((res) => {
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }
  backToList() {
    this.router.navigate(["/pages/account-management/public-holiday/list"]);
  }

  receiverStartTime(event){
    // console.log(this.appService.covertTime(event));
    console.log(this.holidayModel.holiday);


     this.holidayModel.holiday = this.appService.covertTime(event);
  }

}

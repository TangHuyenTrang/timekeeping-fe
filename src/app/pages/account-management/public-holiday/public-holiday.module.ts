import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//import { BrowserModule } from '@angular/platform-browser';

import { Ng2SmartTableModule} from 'ng2-smart-table';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { NbMomentDateModule, NbMomentDateService } from '@nebular/moment';

import { PublicHolidayRoutingModule } from './public-holiday-routing.module';
import { PublicHolidayListComponent } from './public-holiday-list/public-holiday-list.component';
import { PublicHolidayEditComponent } from './public-holiday-edit/public-holiday-edit.component';
import { PublicHolidayAddComponent } from './public-holiday-add/public-holiday-add.component';
import { SharedModule } from '../../../shared/shared.module';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { PublicHolidayService } from '../../../common/services/public-holiday/public-holiday.service';
import { AppService } from '../../../common/services/AppService';
import { NbDateFnsDateModule } from '@nebular/date-fns';


@NgModule({
  declarations: [PublicHolidayListComponent, PublicHolidayEditComponent, PublicHolidayAddComponent],
  imports: [
    CommonModule,
    PublicHolidayRoutingModule,
    CommonModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
   // NbDatepickerModule,
    NbMomentDateModule,
    NbDateFnsDateModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    NotificationsService,
    PublicHolidayService,
  ],
})
export class PublicHolidayModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicHolidayEditComponent } from './public-holiday-edit/public-holiday-edit.component';
import { PublicHolidayListComponent } from './public-holiday-list/public-holiday-list.component';
import { PublicHolidayAddComponent } from './public-holiday-add/public-holiday-add.component';


const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: PublicHolidayEditComponent,
    },
    {
      path: 'list',
      component: PublicHolidayListComponent,
    },
    {
      path: 'add',
      component: PublicHolidayAddComponent,
    }
  ],
}];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicHolidayRoutingModule { }

import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService, NbDateService } from "@nebular/theme";
import { NavigationEnd, Router, ActivatedRoute } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { ItemcostService } from "../../../../common/services/item-cost/itemcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { PublicHolidayService } from '../../../../common/services/public-holiday/public-holiday.service';
import { AppService } from '../../../../common/services/AppService';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-public-holiday-edit',
  templateUrl: './public-holiday-edit.component.html',
  styleUrls: ['./public-holiday-edit.component.scss']
})
export class PublicHolidayEditComponent implements OnInit {

  holidayModel = {
    id: 0,
    holiday: '',
    description: "",
    status: 0,
   }
  constructor(
    private holidayService : PublicHolidayService,
    private notificationsService: NotificationsService,
    private router: Router,
    private appService: AppService,
    public datepipe: DatePipe,
    protected dateService: NbDateService<Date>,
    private routerActive: ActivatedRoute,
    ) {
      this.holidayModel.id = +this.routerActive.snapshot.paramMap.get("id");

    if (this.holidayModel.id !=0) {
      this.getDetail();
    }
    }
    public timeStart:any = '';
     ;

  ngOnInit() {}


  getDetail() {
    this.holidayService
      .getById(this.holidayModel.id)
      .subscribe((result) => {
        this.timeStart = result.Data.Result.Holiday;
        this.holidayModel.holiday = result.Data.Result.Holiday;
        this.holidayModel.description = result.Data.Result.Description;
        this.holidayModel.status = result.Data.Result.Status;
      });
  }
  updateHoliday() {
    this.holidayService.update(this.holidayModel).subscribe((res) => {
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.backToList();
    });
  }



  backToList() {
    this.router.navigate(["/pages/account-management/public-holiday/list"]);
  }

  receiverStartTime(event){
    // console.log(this.appService.covertTime(event));
    console.log(this.holidayModel.holiday);


     this.holidayModel.holiday = this.appService.covertTime(event);
  }

}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicHolidayEditComponent } from './public-holiday-edit.component';

describe('PublicHolidayEditComponent', () => {
  let component: PublicHolidayEditComponent;
  let fixture: ComponentFixture<PublicHolidayEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicHolidayEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicHolidayEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { ItemcostService } from "../../../../common/services/item-cost/itemcost.service";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { PublicHolidayService } from '../../../../common/services/public-holiday/public-holiday.service';

@Component({
  selector: 'ngx-public-holiday-list',
  templateUrl: './public-holiday-list.component.html',
  styleUrls: ['./public-holiday-list.component.scss']
})
export class PublicHolidayListComponent implements OnInit {

  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private holidayService : PublicHolidayService,
    private notificationsService: NotificationsService
  ) {}


  settings = {
    hideSubHeader: true, // hide filter row
    actions: false,
    columns: {
      Holiday: {
        title: "Ngày nghỉ",
        type: "string",
      },
      Description: {
        title: "Mô tả",
        type: "string",
      },
      Status: {
        title: "Loại",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
          // instance.viewdetail.subscribe((row) => {
          //   this.gotoEditRow(row);
          // });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };
  data = [];

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();
  getData() {
    this.source.load([]);
    // this.accountService.getAccount(this.searchModel.hoten, this.searchModel.email,
    //   this.searchModel.unitId, this.searchModel.roleId).subscribe(
    //     result => {
    //       this.data = result.data.results;
    //       // this.source.load(result.data.results);
    //      // this.mergeData();
    //       this.source.load(this.data);
    //     }
    //   )
  }

  getAll() {
    this.source.load([]);
    this.holidayService.getAll().subscribe((result: any) => {
      this.source.load(result.Data.Result);
    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/public-holiday/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa ngày nghỉ" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.holidayService.delete(row.Id).subscribe((res) => {
            if (!res.Success) {
              this.notificationsService.error("Xóa", res.Message);
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa user thành công");
          });
        }
      });
  }

  updateAddNew() {
    this.router.navigate(["/pages/account-management/public-holiday/add"]);
  }
}

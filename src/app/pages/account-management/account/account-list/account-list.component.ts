import { Subscription } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { ActionsColumnComponent } from "../../../../shared/actions-column/actions-column.component";
import { ConfirmationComponent } from "../../../../shared/confirmation/confirmation.component";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { AccountService } from "../../../../common/services/account/account.service";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { AccountStatusComponent } from "../account-status/account-status.component";

@Component({
  selector: "ngx-account-list",
  templateUrl: "./account-list.component.html",
  styleUrls: ["./account-list.component.scss"],
})
export class AccountListComponent implements OnInit {
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private accountService: AccountService,
    private notificationsService: NotificationsService
  ) {}

  searchModel = {
    hoten: "",
    email: "",
    unitId: 0,
    roleId: 0,
  };

  lstRole = [
    { id: 1, name: "Quyền 1" },
    { id: 2, name: "Quyền 2" },
    { id: 3, name: "Quyền 3" },
  ];
  //lstRole = [];
  settings = {
    hideSubHeader: false, // show filter row
    actions: false,
    columns: {
      UserName: {
        title: "User Name",
        type: "string",
      },
      FullName: {
        title: "Full Name",
        type: "string",
      },
      Email: {
        title: "Email",
        type: "string",
      },
      BirthDay: {
        title: "Ngày sinh",
        type: "string",
      },
      DateCreated: {
        title: "Ngày tạo",
        type: "string",
      },

      Level: {
        title: "Cấp bậc",
        type: "string",
      },
      NetSalary: {
        title: "Lương cứng",
        type: "string",
      },
      PhoneNumber: {
        title: "SDT",
        type: "string",
      },
      Special: {
        title: "Chuyên môn",
        type: "string",
      },

      Role: {
        title: "Quyền",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "200px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
          // instance.viewdetail.subscribe((row) => {
          //   this.gotoEditRow(row);
          // });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };
  data = [];

  ngOnInit() {
    this.getAll();
  }

  source: any = new LocalDataSource();
  getData() {
    this.source.load([]);
    // this.accountService.getAccount(this.searchModel.hoten, this.searchModel.email,
    //   this.searchModel.unitId, this.searchModel.roleId).subscribe(
    //     result => {
    //       this.data = result.data.results;
    //       // this.source.load(result.data.results);
    //      // this.mergeData();
    //       this.source.load(this.data);
    //     }
    //   )
  }

  getAll() {
    this.source.load([]);
    this.accountService.getAll().subscribe((result: any) => {
      console.log(result.Data);
      this.source.load(result.Data);
    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/account-management/account/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa nhân sự" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.accountService.deleteAccount(row.Id).subscribe((res) => {
            if (!res.Success) {
              this.notificationsService.error("Xóa", res.Message);
              return;
            }
            this.getAll();
            this.notificationsService.success("Xóa", "Xóa user thành công");
          });
        }
      });
  }

  updateAddNew() {
    this.router.navigate(["/pages/account-management/account/add"]);
  }

  // mergeData() {
  //   this.data.forEach(element => {
  //     debugger;
  //     element.roleName = this.getRoleNameById(element.roleId);
  //     element.unitName = this.getUnitNameById(element.unitId);
  //   });
  // }

  // getRoleNameById(id: number) {

  //   let i;
  //   let lenght = this.lstRole.length;
  //   for (i = 0; i < lenght; i++) {
  //     if (this.lstRole[i].id == id) {
  //       return this.lstRole[i].name;
  //     }
  //   }
  //   return '';
  // }
}

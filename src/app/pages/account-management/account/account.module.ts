import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AccountRoutingModule } from './account-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { AccountEditComponent } from './account-edit/account-edit.component';
import { AccountAddComponent } from './account-add/account-add.component';
import { AccountListComponent } from './account-list/account-list.component';
import { CommonModule } from '../../../common/common.module';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { AccountService } from '../../../common/services/account/account.service';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AccountStatusComponent } from '../account/account-status/account-status.component';
import { NbDateFnsDateModule } from '@nebular/date-fns';
@NgModule({
  imports: [
    AccountRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbDatepickerModule,
    NbDateFnsDateModule,
  ],
  declarations: [
    AccountEditComponent,
    AccountListComponent,
    AccountAddComponent,
    AccountStatusComponent,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    AccountService,
    NotificationsService,
  ],
  entryComponents: [
    AccountStatusComponent,
  ],
})
export class AccountModule {
}

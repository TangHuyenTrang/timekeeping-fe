import { RoleService } from './../../../../common/services/account/role.service';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { DataStorage } from '../../../../common/data/data-storage';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AccountService } from '../../../../common/services/account/account.service';
import { eventNames } from 'cluster';

@Component({
  selector: 'ngx-account-add',
  templateUrl: './account-add.component.html',
  styleUrls: ['./account-add.component.scss']
})
export class AccountAddComponent implements OnInit {

  listPerSelect: any = [];
  listPerGroup: any = [];
  accountModel = {
    id: '',
    userName: '',
    fullName: '',
    email: '',
    passwordHash: '',
    birthday: '',
    level: '',
    netSalary: 0,
    mandaySalary: 0,
    special: '',
    skillId: 0,
    phoneNumber: '',
    specializationId: 0,
    roleId: '',
  };
  lstSpecial = [];
  lstRole = [];
  constructor(private accountService: AccountService, private router: Router, private roleService: RoleService,
    private notificationsService: NotificationsService,
    ) { }
  ngOnInit() {
    this.accountService.getSpecials().subscribe(
      (result:any) => {
          this.lstSpecial = result.Data;
      }
   )
   this.roleService.getRole().subscribe(
    (result:any) => {
        this.lstRole = result.Data;
    }
 )
  }

  addAccount(){
    if(this.accountModel.userName == '')
    {
      this.notificationsService.error('Validation', "Thiếu tên tài khoản");
      return;
    }
    if(this.accountModel.roleId == '')
    {
      this.notificationsService.error('Validation', "Chưa chọn quyền");
      return;
    }
    if(this.accountModel.passwordHash == '')
    {
      this.notificationsService.error('Validation', "Thiếu mật khẩu");
      return;
    }
    if(this.accountModel.email == '')
    {
      this.notificationsService.error('Validation', "Thiếu email");
      return;
    }
    if(this.accountModel.netSalary == 0)
    {
      this.notificationsService.error('Validation', "Thiếu lương cơ bản");
      return;
    }
    if(this.accountModel.mandaySalary == 0)
    {
      this.notificationsService.error('Validation', "Thiếu lương Manday");
      return;
    }
    if(this.accountModel.birthday == '')
    {
      this.notificationsService.error('Validation', "Chưa chọn ngày sinh");
      return;
    }
    this.accountService.addAccount(this.accountModel).subscribe(res =>{
      console.log(22,res);
      if(!res.Success) {
        this.notificationsService.error('Cập nhật', res.Message);
        return;
      }
      this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
      this.router.navigate(['/pages/account-management/account/list']);
  });
}

  backToList() {
    this.router.navigate(['/pages/account-management/account/list']);
  }

  testChange(event){
     var year = event.getFullYear();
     var month = event.getMonth() + 1;
     var date = event.getDate();
    if(month < 10)
    {
      month = `0${month}`;
    }
    if(date < 10)
    {
      date = `0${date}`;
    }
    return `${date}/${month}/${year}`;
  }
}

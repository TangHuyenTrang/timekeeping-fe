import { Subscription } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { DataStorage } from "../../../../common/data/data-storage";
import { NotificationsService } from "../../../../common/services/notifications/notifications.service";
import { RoleService } from "./../../../../common/services/account/role.service";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { AccountService } from "../../../../common/services/account/account.service";
import { AppService } from "./../../../../common/services/AppService";

@Component({
  selector: "ngx-account-edit",
  templateUrl: "./account-edit.component.html",
  styleUrls: ["./account-edit.component.scss"],
})
export class AccountEditComponent implements OnInit {
  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private accountService: AccountService,
    private router: Router,
    private roleService: RoleService,
    private appService: AppService
  ) {
    this.accountModel.id = this.routerActive.snapshot.paramMap.get("id");

    if (this.accountModel.id != "0") {
      this.getAccountDetail();
    }
    //this.getAccountDetail();
  }

  accountModel = {
    id: "0",
    userName: "",
    fullName: "",
    email: "",
    password: "",
    birthday: "",
    dateCreated: "",
    level: "",
    netSalary: 0,
    mandaySalary: 0,
    special: "",
    skill: "",
    phoneNumber: "",
    specializationId: 0,
    roleId: "",
  };

  lstSpecial = [];
  lstRole = [];
  time: any = "";
  newBirthday: any = "";

  ngOnInit() {
    this.getAccountDetail();
    this.accountService.getSpecials().subscribe((result: any) => {
      this.lstSpecial = result.Data;
    });
    this.roleService.getRole().subscribe((result: any) => {
      this.lstRole = result.Data;
    });
  }
  getAccountDetail() {
    this.accountService
      .getAccountById(this.accountModel.id)
      .subscribe((result) => {
        this.accountModel.fullName = result.Data.FullName;
        this.accountModel.birthday = result.Data.BirthDay;
        this.accountModel.email = result.Data.Email;
        this.accountModel.phoneNumber = result.Data.PhoneNumber;
        this.accountModel.userName = result.Data.UserName;
        this.accountModel.dateCreated = result.Data.DateCreated;
        this.accountModel.skill = result.Data.SkillName;
        this.accountModel.netSalary = result.Data.NetSalary;
        this.accountModel.mandaySalary = result.Data.MandaySalary;
        this.accountModel.special = result.Data.Special;
        this.accountModel.level = result.Data.Level;
        this.accountModel.roleId = result.Data.RoleId;
        this.time = this.appService.formatTimeDisplayNebular(
          this.accountModel.dateCreated
        );
        this.newBirthday = this.appService.formatTimeDisplayNebular(
          this.accountModel.birthday
        );
      });
  }

  updateAccount() {
    console.log(1, this.accountModel);
    this.accountService.addUpdate(this.accountModel).subscribe((res) => {

      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      this.router.navigate(["/pages/account-management/account/list"]);
    });
  }

  backToList() {
    this.router.navigate(["/pages/account-management/account/list"]);
  }

  receiverDate(event) {
    this.accountModel.dateCreated = this.appService.covertTime(event);
  }
  receiverBirthday(event) {
    this.accountModel.birthday = this.appService.covertTime(event);
  }
}

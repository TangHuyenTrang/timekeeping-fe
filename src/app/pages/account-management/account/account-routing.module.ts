import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AccountEditComponent } from './account-edit/account-edit.component';
import { AccountListComponent } from './account-list/account-list.component';
import { AccountAddComponent } from './account-add/account-add.component';
const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit/:id',
      component: AccountEditComponent,
    },
    {
      path: 'list',
      component: AccountListComponent,
    },
    {
      path: 'add',
      component: AccountAddComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {
}

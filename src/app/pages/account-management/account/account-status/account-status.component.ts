import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ngx-account-status',
  templateUrl: './account-status.component.html',
  styleUrls: ['./account-status.component.scss']
})
export class AccountStatusComponent implements OnInit {

  @Input() rowData: any;
  status = 1;
  constructor() { }
  isDisplay1: boolean;
  isDisplay2: boolean;
  isDisplay3: boolean;

  ngOnInit() {

    status = this.rowData.status;
    if(status == '1' )
    {
      this.isDisplay1 = true;
    }else if(status == '2'){
      this.isDisplay2 = true;
    }else{
      this.isDisplay3 = true;
    }
  }

}

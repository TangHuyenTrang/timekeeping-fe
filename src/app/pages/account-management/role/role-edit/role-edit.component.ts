import { Component, OnInit } from '@angular/core';
import { DataStorage } from '../../../../common/data/data-storage';
import { RoleService } from '../../../../common/services/account/role.service';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'ngx-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss']
})
export class RoleEditComponent implements OnInit {

  listPer: any = [
  ]

  listGroup : any = [
    { "id": 1, "name": "Quản lý quyền" },
    { "id": 2, "name": "Quản lý tài khoản" },
    { "id": 3, "name": "Quản lý giá điện" },
    { "id": 4, "name": "Quản lý ca làm việc" },
    { "id": 5, "name": "Quản lý đối tượng" }
  ]

  listPerSelect: any = [];
  listPerGroup: any = [];
  rolePermission: any = {
    id: 0,
    roleName: "",
    roleDescription: "",
    permissionList: [
    ]
  }

  constructor(private roleService: RoleService, private router: Router,
    private notificationsService: NotificationsService,) { }
  ngOnInit() {
    // this.listPerGroup = this.listGroup.map(item => {
    //   var listChild = this.listPer.filter(itemper => itemper.groupId == item.Id);
    //   item.child = listChild;
    //   return item;
    // });
    this.getRoleDetail();
  }

  getRoleDetail() {
    var roleId = DataStorage.getParameter("id");
    this.roleService.getRoleById(roleId).subscribe(
      result => {
        if(result.success) {
          this.rolePermission = result.data;
          if(this.rolePermission.listPermission) {
            this.listPerSelect = this.rolePermission.listPermission.map(x => {
              return x.id;
            });
          }
          this.getListRole();
        }
      }
    );
  }

  getListRole() {
    this.roleService.getAllPermission().subscribe(
      result => {
        if (result.success) {
          this.listPer = result.data;
          this.listPerGroup = this.listGroup.map(item => {
            var listChild = this.listPer.filter(itemper => itemper.groupId == item.id);
            item.child = listChild;
            return item;
          });
        }
      }
    );
  }

  updateRole() {
    this.rolePermission.permissionList = this.listPerSelect.map(x => {
      return {
        permissionId: x
      }
    });
    this.roleService.editRole(this.rolePermission).subscribe(res => {
      if(!res.success) {
        this.notificationsService.error('Cập nhật', res.Message);
        return;
      }
      this.notificationsService.success('Cập nhật', 'Cập nhật thành công');
      setTimeout(() => {
        this.router.navigate(['/pages/account-management/role/list']);
      }, 1000);
    });
  }

  changeSelected(isChecked, per) {
    let indexper = this.listPerSelect.findIndex(x => x.Id == per.id);
    if (isChecked) {
      if (indexper < 0) {
        this.listPerSelect.push(per.id);
      }
      return;
    }
    this.listPerSelect = this.listPerSelect.filter(x => x != per.id);
  }

  checkSelected(per) {
    let index = this.listPerSelect.findIndex(x => x == per.id);
    if (index >= 0) {
      return true;
    }
    return false;
  }


  gotoListRole() {
    this.router.navigate(['/pages/account-management/role/list']);
  }

}

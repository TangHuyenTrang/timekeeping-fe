import { Component, OnInit } from '@angular/core';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { ActionsColumnComponent } from '../../../../shared/actions-column/actions-column.component';
import { ConfirmationComponent } from '../../../../shared/confirmation/confirmation.component';
import { NbDialogService } from '@nebular/theme';
import { NavigationEnd, Router } from '@angular/router';
import { RoleService } from '../../../../common/services/account/role.service';
import { NotificationsService } from '../../../../common/services/notifications/notifications.service';
@Component({
  selector: 'ngx-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnInit {
  settings = {
    hideSubHeader: true, // hide filter row
    actions: false,
    columns: {
      id: {
        title: 'Id',
        type: 'number',
      },
      name: {
        title: 'Quyền',
        type: 'string',
      },
      description: {
        title: 'Mô tả',
        type: 'string',
      },
      actions: {
        title: 'Thao Tác',
        type: 'custom',
        width: '150px',
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe(row => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe(row => {
            this.gotoDeleteRow(row);
          });
          instance.viewdetail.subscribe(row => {
            this.gotoEditRow(row);
          });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };
  data = [
  ]

  source: LocalDataSource = new LocalDataSource();


  constructor(private dialogService: NbDialogService, private router: Router,
    private roleService: RoleService, private notificationsService: NotificationsService) {
    this.getData();
  }

  ngOnInit() {

  }

  gotoEditRow(row) {
    this.router.navigate(['/pages/account-management/role/edit'],{ queryParams: { id: row.id }});
  }

  gotoDeleteRow(row) {
    this.dialogService.open(ConfirmationComponent, { context: { type: 'Xác nhận xóa quyền' } }).onClose.subscribe(
      confirm => {
        if (confirm) {
          this.roleService.deleteRole(row.id).subscribe(res => {
            if (!res.success) {
              this.notificationsService.error('Xóa', res.Message);
              return;
            }
            this.notificationsService.success('Xóa', 'Xóa quyền thành công');
            this.getData();
          });
        }
      },
    );
  }


  getData() {
    this.source.load([]);
    this.roleService.getRole().subscribe(
      result => {
        this.source.load(result.data);
      }
    )
  }

  updateAddNew() {
    this.router.navigate(['/pages/account-management/role/add']);
  }

}

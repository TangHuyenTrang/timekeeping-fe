import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { RoleEditComponent } from './role-edit/role-edit.component';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleAddComponent } from './role-add/role-add.component';
const routes: Routes = [{
  path: '',
  children: [
    {
      path: 'edit',
      component: RoleEditComponent,
    },
    {
      path: 'list',
      component: RoleListComponent,
    },
    {
      path: 'add',
      component: RoleAddComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoleRoutingModule {
}

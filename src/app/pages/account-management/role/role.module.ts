import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RoleRoutingModule } from './role-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { RoleEditComponent } from './role-edit/role-edit.component';
import { RoleAddComponent } from './role-add/role-add.component';
import { RoleListComponent } from './role-list/role-list.component';
import { CommonModule } from '../../../common/common.module';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { RoleService } from '../../../common/services/account/role.service';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbCheckboxModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
@NgModule({
  imports: [
    RoleRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    CommonModule,
    FormsModule
  ],
  declarations: [
    RoleEditComponent,
    RoleListComponent,
    RoleAddComponent
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    RoleService,
    NotificationsService
  ],
})
export class RoleModule {
}

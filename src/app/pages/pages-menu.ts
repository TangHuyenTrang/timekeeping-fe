import { Injectable } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

// export const MENU_ITEMS: NbMenuItem[] = [

//   {
//     title: 'Thư viện',
//     icon: 'people-outline',
//     link: '/pages/iot-dashboard',
//     children: [
//       {
//         title: 'Thư viện nhân sự',
//         link: '/pages/account-management/account/list',
//       },
//       {
//         title: 'Thư viện dự án',
//         link: '/pages/account-management/project/list',
//       },
//       {
//         title: 'Thư viện nhóm chi phí',
//         link: '/pages/account-management/group-cost/list',
//       },
//       {
//         title: 'Thư viện dịch vụ',
//         link: '/pages/account-management/serv/list',
//       },
//       {
//         title: 'Thư viện chuyên môn',
//         link: '/pages/account-management/specializa/list',
//       },
//       {
//         title: 'Thư viện khoản mục',
//         link: '/pages/account-management/item-cost/list',
//       }
//     ],
//   },



//   {
//     title: 'Chi phí',
//     icon: 'keypad-outline',
//     link: '/pages/ui-features',
//     children: [
//       {
//         title: 'Chia nhóm khoản thu chi',
//         link: '/pages/cost-management/accouting-distribution/list',
//       },
//       {
//         title: 'Duyệt khoản thu chi',
//         link: '/pages/cost-management/approval-expense/list',
//       },
//     ],
//    },
//   {
//     title: 'Chấm công',
//     icon: 'browser-outline',
//     link: '/pages/timekeeping/list',
//   },
//   {
//     title: 'Báo cáo',
//     icon: 'edit-2-outline',

//   },

//   {
//     title: 'Phân quyền',
//     icon: 'lock-outline',

//   },



// ];

@Injectable()
export class MenuItems {
  public MENU_ITEMS: NbMenuItem[] = [
    {
      title: "Thư viện",
      icon: "people-outline",
      link: "/pages/iot-dashboard",
      children: [
        {
          title: "Thư viện nhân sự",
          link: "/pages/account-management/account/list",
        },
        {
          title: "Thư viện dự án",
          link: "/pages/account-management/project/list",
        },
        {
          title: "Thư viện nhóm chi phí",
          link: "/pages/account-management/group-cost/list",
        },
        {
          title: "Thư viện dịch vụ",
          link: "/pages/account-management/serv/list",
        },
        {
          title: "Thư viện chuyên môn",
          link: "/pages/account-management/specializa/list",
        },
        {
          title: "Thư viện khoản mục",
          link: "/pages/account-management/item-cost/list",
        },
      ],
    },

    {
      title: "Chi phí",
      icon: "keypad-outline",
      link: "/pages/ui-features",
      children: [
        {
          title: "Chia nhóm khoản thu chi",
          link: "/pages/cost-management/accouting-distribution/list",
        },
        {
          title: "Duyệt khoản thu chi",
          link: "/pages/cost-management/approval-expense/list",
        },
      ],
    },
    {
      title: "Chấm công",
      icon: "browser-outline",
      link: "/pages/timekeeping/list",
    },
    {
      title: "Báo cáo",
      icon: "edit-2-outline",
    },

    {
      title: "Phân quyền",
      icon: "lock-outline",
    },
  ];

}


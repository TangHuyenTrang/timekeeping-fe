import { Component, OnInit } from '@angular/core';
import { TimekeepingService } from '../../../common/services/timekeeping/timekeeping.service';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { Router } from '@angular/router';
import { AppService } from '../../../common/services/AppService';
import { NbDateService, NbThemeService } from '@nebular/theme';
import { DatePipe } from '@angular/common';

import 'rxjs/Rx' ;
@Component({
  selector: 'ngx-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.scss']
})
export class ReportListComponent implements OnInit {
  public timeStart:any = ''; // Biến trung gian để hiểện thị time lên time Nebular
  public timeEnd:any = '';
  baoCaoSelected: any;
  model = {
    startDate: '',
    endDate: '',
  };
  options: any = {};
  themeSubscription: any;



  constructor(
    private timeKeepingService : TimekeepingService,
    private notificationsService: NotificationsService,
    private router: Router,
    private appService:AppService,
    protected dateService: NbDateService<Date>,
    public datepipe: DatePipe,
    private theme: NbThemeService
  ) {

  }


  ngOnInit() {

  }

  getData() {
  }


  xuatFile(){
    this.timeKeepingService.tinhLuong(this.model.startDate, this.model.endDate).then((res) =>{
    this.downLoadFile(res, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export_bang_luong" + ".xlsx")
    this.notificationsService.success('', 'Xuất file thành công');
  });
}


xuatFileChiPhi(){
  this.timeKeepingService.tinhChiPhi(this.model.startDate, this.model.endDate).then((res) =>{
  this.downLoadFile(res, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Export_chi_phi" + ".xlsx")
  this.notificationsService.success('', 'Xuất file thành công');
});
}


downLoadFile(data: any, type: string, filename) {
  console.log('nrnr');

  let blob = new Blob([data], { type: type });
  let url = window.URL.createObjectURL(blob);

  // create hidden dom element (so it works in all browsers)
  const a = document.createElement('a');
  a.setAttribute('style', 'display:none;');
  document.body.appendChild(a);

  // create file, attach to hidden element and open hidden element
  a.href = url;
  a.download = filename;
  a.click();
}
  receiverStartTime(event){
    this.model.startDate = this.appService.covertTime(event);
  }

  receiverEndTime(event){
    this.model.endDate = this.appService.covertTime(event);
  }


  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {

      const colors = config.variables;
      const echarts: any = config.variables.echarts;

      this.options = {
        backgroundColor: echarts.bg,
        color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)',
        },
        legend: {
          orient: 'vertical',
          left: 'left',
          data: ['USA', 'Germany', 'France', 'Canada', 'Russia'],
          textStyle: {
            color: echarts.textColor,
          },
        },
        series: [
          {
            name: 'Countries',
            type: 'pie',
            radius: '80%',
            center: ['50%', '50%'],
            data: [
              { value: 335, name: 'Germany' },
              { value: 310, name: 'France' },
              { value: 234, name: 'Canada' },
              { value: 135, name: 'Russia' },
              { value: 1548, name: 'USA' },
            ],
            itemStyle: {
              emphasis: {
                shadowBlur: 10,
                shadowOffsetX: 0,
                shadowColor: echarts.itemHoverShadowColor,
              },
            },
            label: {
              normal: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
            labelLine: {
              normal: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
            },
          },
        ],
      };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}

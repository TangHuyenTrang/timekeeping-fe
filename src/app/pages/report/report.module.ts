import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { ReportListComponent } from './report-list/report-list.component';

import { SharedModule } from '../../shared/shared.module';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { ConfirmationComponent } from '../../shared/confirmation/confirmation.component';

import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { NotificationsService } from '../../common/services/notifications/notifications.service';
import { NbMomentDateModule } from '@nebular/moment';
import { FormsModule } from '@angular/forms';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { NgxChartsModule } from '@swimlane/ngx-charts';
//import { ChartsModule } from '../charts/charts.module';

@NgModule({
  declarations: [ReportListComponent],
  imports: [
    CommonModule,
    ReportRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbMomentDateModule,
    NbDatepickerModule,
    NbDateFnsDateModule,
   // ChartsModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    NotificationsService
  ],
})
export class ReportModule { }

import { AuthGuard } from './../helpers/auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: '',
      canActivate: [AuthGuard],
      component: ECommerceComponent,
    },
    {
      path: 'iot-dashboard',
      component: DashboardComponent,
    },

    {
      path: 'charts',
      canActivate: [AuthGuard],
      loadChildren: () => import('./charts/charts.module')
        .then(m => m.ChartsModule),
    },

    {
      path: 'miscellaneous',
      canActivate: [AuthGuard],
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },

    {
      path: 'account-management',
      canActivate: [AuthGuard],
      loadChildren: () => import('./account-management/account-management.module')
        .then(m => m.AccountManagementModule),
    },
    {
      path: 'cost-management',
      canActivate: [AuthGuard],
      loadChildren: () => import('./cost-management/cost-management.module')
        .then(m => m.CostManagementModule),
    },
    {
      path: 'timekeeping',
      canActivate: [AuthGuard],
      loadChildren: () => import('./timekeeping/timekeeping.module')
        .then(m => m.TimekeepingModule),
    },
    {
      path: 'report',
      canActivate: [AuthGuard],
      loadChildren: () => import('./report/report.module')
        .then(m => m.ReportModule),
    },
    {
      path: 'project-task',
      canActivate: [AuthGuard],
      loadChildren: () => import('./project-task/project-task.module')
        .then(m => m.ProjectTaskModule),
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}

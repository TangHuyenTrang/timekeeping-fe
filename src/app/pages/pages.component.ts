import { AppService } from "./../common/services/AppService";
import { Component } from "@angular/core";

// import { MENU_ITEMS } from "./pages-menu";

@Component({
  selector: "ngx-pages",
  styleUrls: ["pages.component.scss"],
  template: ` <ngx-one-column-layout>
    <nb-menu [items]="menu"></nb-menu>
    <router-outlet></router-outlet>
  </ngx-one-column-layout>`,
})
export class PagesComponent {
  public menu: any[] = [];
  constructor(private _AppService: AppService) {
    console.log(this._AppService.MenuItems);
    this._AppService.getListPerUser().subscribe((res) => {
      if(res.Data != null)
      {
        this.formatItemMenu(res.Data.Result);
      }
      else
      {
        console.log("Oops...Error Server")
      }
    });
  }

  formatItemMenu(listPermission: any[]) {
    const MenuItems: any = [];
    console.log(listPermission);

    if (Array.isArray(listPermission) && listPermission.length > 0) {
      listPermission.forEach((dataPermission: string) => {
        if (dataPermission == "view_library") {
          MenuItems.push({
            title: "Thư viện",
            icon: "people-outline",
            link: "/pages/iot-dashboard",
            children: [
              {
                title: "Thư viện nhân sự",
                link: "/pages/account-management/account/list",
              },
              {
                title: "Thư viện dự án",
                link: "/pages/account-management/project/list",
              },
              {
                title: "Thư viện nhóm chi phí",
                link: "/pages/account-management/group-cost/list",
              },
              {
                title: "Thư viện dịch vụ",
                link: "/pages/account-management/serv/list",
              },
              {
                title: "Thư viện chuyên môn",
                link: "/pages/account-management/specializa/list",
              },
              {
                title: "Thư viện khoản mục",
                link: "/pages/account-management/item-cost/list",
              },
              {
                title: "Thiết lập ngày nghỉ lễ",
                link: "/pages/account-management/public-holiday/list",
              },
            ],
          });
        }
      });
      this.menu = MenuItems;
    }
    if (Array.isArray(listPermission) && listPermission.length > 0) {
      listPermission.forEach((dataPermission: string) => {
        if (dataPermission == "chianhom") {
          let exits: boolean = false;
          if (Array.isArray(MenuItems) && MenuItems.length > 0) {
            MenuItems.forEach((dataMenu: any) => {
              if (dataMenu.title == "Chi phí") {
                exits = true;
                dataMenu.children.push({
                  title: "Chia nhóm khoản thu chi",
                  link: "/pages/cost-management/accouting-distribution/list",
                });
              }
            });
          }
          if (!exits) {
            MenuItems.push({
              title: "Chi phí",
              icon: "keypad-outline",
              link: "/pages/ui-features",
              children: [
                {
                  title: "Chia nhóm khoản thu chi",
                  link: "/pages/cost-management/accouting-distribution/list",
                },
              ],
            });
          }
        }
      });
    }
    if (Array.isArray(listPermission) && listPermission.length > 0) {
      listPermission.forEach((dataPermission: string) => {
        if (dataPermission == "duyetthuchi") {
          let exits: boolean = false;
          if (Array.isArray(MenuItems) && MenuItems.length > 0) {
            MenuItems.forEach((dataMenu: any) => {
              if (dataMenu.title == "Chi phí") {
                exits = true;

                dataMenu.children.push({
                  title: "Duyệt khoản thu chi",
                  link: "/pages/cost-management/approval-expense/list",
                });
              }
            });
          }
          if (!exits) {
            MenuItems.push({
              title: "Chi phí",
              icon: "keypad-outline",
              link: "/pages/ui-features",
              children: [
                {
                  title: "Duyệt khoản thu chi",
                  link: "/pages/cost-management/approval-expense/list",
                },
              ],
            });
          }
        }
      });
    }
    if (Array.isArray(listPermission) && listPermission.length > 0) {
      listPermission.forEach((dataPermission: string) => {
        if (dataPermission == "chiphiketoan") {
          let exits: boolean = false;
          if (Array.isArray(MenuItems) && MenuItems.length > 0) {
            MenuItems.forEach((dataMenu: any) => {
              if (dataMenu.title == "Chi phí") {
                exits = true;

                dataMenu.children.push({
                  title: "Chi phí kế toán",
                  link: "/pages/cost-management/cost/list",
                });
              }
            });
          }
          if (!exits) {
            MenuItems.push({
              title: "Chi phí",
              icon: "keypad-outline",
              link: "/pages/ui-features",
              children: [
                {
                  title: "Chi phí kế toán",
                  link: "/pages/cost-management/cost/list",
                },
              ],
            });
          }
        }
      });
    }
    if (Array.isArray(listPermission) && listPermission.length > 0) {
      listPermission.forEach((dataPermission: string) => {
        if (dataPermission == "chamcong") {
          MenuItems.push({
            title: "Chấm công",
            icon: "browser-outline",
            link: "/pages/timekeeping/list",
          });
        }
      });
      this.menu = MenuItems;
    }
    if (Array.isArray(listPermission) && listPermission.length > 0) {
      listPermission.forEach((dataPermission: string) => {
        if (dataPermission == "baocao") {
          MenuItems.push({
            title: "Báo cáo",
            icon: "browser-outline",
            link: "/pages/report/list",
          });
        }
      });
      this.menu = MenuItems;
    }
    // if (Array.isArray(listPermission) && listPermission.length > 0) {
    //   listPermission.forEach((dataPermission: string) => {
    //     if (dataPermission == "phanquyen") {
    //       MenuItems.push({
    //         title: "Phân quyền",
    //         icon: "browser-outline",
    //         link: "/pages/timekeeping/list",
    //       });
    //     }
    //   });
    //   this.menu = MenuItems;
    // }
    if (Array.isArray(listPermission) && listPermission.length > 0) {
      listPermission.forEach((dataPermission: string) => {
        if (dataPermission == "xemtask") {
          MenuItems.push({
            title: "Công việc của tôi",
            icon: "browser-outline",
            link: "/pages/project-task/list",
          });
        }
      });
      this.menu = MenuItems;
    }
  }
}

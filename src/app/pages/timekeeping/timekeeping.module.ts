import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//import { BrowserModule } from '@angular/platform-browser';

import { TimekeepingRoutingModule } from './timekeeping-routing.module';
import { TimekeepingListComponent } from './timekeeping-list/timekeeping-list.component';
import { TimekeepingAddComponent } from './timekeeping-add/timekeeping-add.component';

import { SharedModule } from '../../shared/shared.module';
import { Ng2SmartTableModule} from 'ng2-smart-table';
import { ConfirmationComponent } from '../../shared/confirmation/confirmation.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
// import { ThemeModule } from 'app/@theme/theme.module';
import {
  NbMenuModule,
  NbDialogService,
  NbWindowService,
  NbListModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbDatepickerModule,
} from '@nebular/theme';
import { NotificationsService } from '../../common/services/notifications/notifications.service';
import { NbMomentDateModule } from '@nebular/moment';
import { TimekeepingDetailComponent } from './timekeeping-detail/timekeeping-detail.component';
import { TimekeepingEditComponent } from './timekeeping-edit/timekeeping-edit.component';
import { TaskService } from '../../common/services/task/task.service';
import { AccountService } from '../../common/services/account/account.service';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { AppService } from '../../common/services/AppService';
@NgModule({
  declarations: [TimekeepingListComponent, TimekeepingAddComponent, TimekeepingDetailComponent, TimekeepingEditComponent],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      //apiKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
      apiKey: 'AIzaSyDL6Y0SB_wpJiQwRXA-gDs3tFOTPY0GVSY',
    }),
    HttpClientModule,
    AngularEditorModule,
    TimekeepingRoutingModule,
    NbMenuModule,
    SharedModule,
    Ng2SmartTableModule,
    NbListModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    NbMomentDateModule,
    NbDatepickerModule,
    NbDateFnsDateModule,
    CKEditorModule,
  ],
  providers: [
    NbDialogService,
    NbWindowService,
    NotificationsService,
    TaskService,
    AccountService,
    AppService
  ],
  entryComponents: [TimekeepingDetailComponent, ConfirmationComponent],
})
export class TimekeepingModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { TimekeepingService } from '../../../common/services/timekeeping/timekeeping.service';
import { NotificationsService } from '../../../common/services/notifications/notifications.service';
import { ProjectService } from '../../../common/services/project/project.service';
import { MouseEvent } from '@agm/core';
import { AppService } from '../../../common/services/AppService';
import { DatePipe } from '@angular/common';
import { NbDateService } from '@nebular/theme';

@Component({
  selector: 'ngx-timekeeping-edit',
  templateUrl: './timekeeping-edit.component.html',
  styleUrls: ['./timekeeping-edit.component.scss']
})
export class TimekeepingEditComponent implements OnInit {
  id: number;
  fileUrl: string;
  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private timekeepingService: TimekeepingService,
    private projectService: ProjectService,
    private router: Router,
    private appService:AppService,
    public datepipe: DatePipe,
    protected dateService: NbDateService<Date>,
    ) {
      this.id = +this.routerActive.snapshot.paramMap.get("id");

    }
    timeKeepingModel = {
      shiftTime: 0,
      workTime: null,
      description: "",
      userId: "",
      image: "",
      document: "",
      projectId: 0,
      status: 2,
      latitude: '',
      longitude: '',
     };


  ngOnInit() {
  }
  file: File;
  onFileChange(event: any) {
    let files = event.target.files;
    if (files.length <= 0) return

    this.file = <File>files[0];
  }

  update() {
    let formData = new FormData();
    formData.append('images', this.file);
    this.timekeepingService.uploadDocument(formData).subscribe((res) => {
      if (res.Success) {
        this.fileUrl = res.Message;
      }
    this.timekeepingService.updateTimeKeeping(this.id, this.fileUrl).subscribe((res) => {
      if (!res.Success) {
        this.notificationsService.error("Cập nhật", res.Message);
        return;
      }
      this.notificationsService.success("Cập nhật", "Cập nhật thành công");
     this.backToList();
    });
  });
}

  backToList() {
    this.router.navigate(["/pages/timekeeping/list"]);
  }
}

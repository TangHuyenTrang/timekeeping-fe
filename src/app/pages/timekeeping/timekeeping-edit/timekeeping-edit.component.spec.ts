import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimekeepingEditComponent } from './timekeeping-edit.component';

describe('TimekeepingEditComponent', () => {
  let component: TimekeepingEditComponent;
  let fixture: ComponentFixture<TimekeepingEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimekeepingEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimekeepingEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TimekeepingListComponent } from './timekeeping-list/timekeeping-list.component';
import { TimekeepingAddComponent } from './timekeeping-add/timekeeping-add.component';
import { TimekeepingEditComponent } from './timekeeping-edit/timekeeping-edit.component';

const routes: Routes = [
  {
    path: "list/:id",
    component: TimekeepingListComponent,
  },
  {
    path: "list",
    component: TimekeepingListComponent,
  },
   {
    path: "add",
    component: TimekeepingAddComponent,
  },
  {
    path: 'edit/:id',
    component: TimekeepingEditComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimekeepingRoutingModule {}

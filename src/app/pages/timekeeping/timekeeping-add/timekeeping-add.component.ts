import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { TimekeepingService } from "../../../common/services/timekeeping/timekeeping.service";
import { NotificationsService } from "../../../common/services/notifications/notifications.service";
import { ProjectService } from "../../../common/services/project/project.service";
import { MouseEvent } from "@agm/core";
import { AppService } from "../../../common/services/AppService";
import { DatePipe } from "@angular/common";
import { NbDateService } from "@nebular/theme";
import { TaskService } from "../../../common/services/task/task.service";

import { AngularEditorConfig } from "@kolkov/angular-editor";

@Component({
  selector: "ngx-timekeeping-add",
  templateUrl: "./timekeeping-add.component.html",
  styleUrls: ["./timekeeping-add.component.scss"],
})
export class TimekeepingAddComponent implements OnInit {
  title: string = "AGM project";

  latitude: number;
  longitude: number;
  zoom: number;
  idProject: number = 0;
  htmlContent = "";
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "15rem",
    minHeight: "5rem",
    placeholder: "Enter text here...",
    translate: "no",
    defaultParagraphSeparator: "p",
    defaultFontName: "Arial",
    toolbarHiddenButtons: [["bold"]],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: "redText",
        class: "redText",
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ],
  };
  constructor(
    private routerActive: ActivatedRoute,
    private notificationsService: NotificationsService,
    private timekeepingService: TimekeepingService,
    private projectService: ProjectService,
    private taskService: TaskService,
    private router: Router,
    private appService: AppService,
    public datepipe: DatePipe,
    protected dateService: NbDateService<Date>
  ) {}
  costModel = {
    flag: 0,
    id: 0,
    currency: 0,
    title: "",
    note: "",
    createDate: new Date(),
    groupCostId: "",
    itemsCostId: "",
    timeKeepingId: 0,
    imageUrl: "",
    type: 1, //nhan vien
    imageFile: null,
  };
  timeKeepingModel = {
    shiftTime: 0,
    workTime: null,
    description: "",
    userId: "",
    image: "",
    document: "",
    projectId: 0,
    taskId: 0,
    status: 0,
    latitude: "",
    code: "",
    longitude: "",
    listCost: [this.costModel],
  };
  lstProject = [];
  lstTask = [];
  anh = "";

  ngOnInit() {
    this.projectService.getAllByUser().subscribe((result: any) => {
      this.lstProject = result.Data;
    });

    this.setCurrentLocation();
  }

  private setCurrentLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 20;
      });
    }
  }

  imageIcon: File;
  onImageIconChange(event: any) {
    let files = event.target.files;
    if (files.length <= 0) return;

    this.imageIcon = <File>files[0];
    console.log(this.imageIcon);
  }

  onImageIconChange_Cost(event: any, flag: number) {
    let files = event.target.files;
    if (files.length <= 0) return;

    let tmp = this.timeKeepingModel.listCost.find((x) => x.flag == flag);
    tmp.imageFile = <File>files[0];

    //upload
  }

  async add() {
    await this.addCosts();

    if (this.timeKeepingModel.projectId == 0) {
      this.notificationsService.error("Cảnh báo", "Bạn chưa chọn dự án");
      return;
    }
    if (this.timeKeepingModel.taskId ==0) {
      this.notificationsService.error("Cảnh báo", "Bạn chưa chọn công việc");
      return;
    }
    if (this.imageIcon == null) {
      this.notificationsService.error("Cảnh báo", "Bạn chưa up ảnh chấm công");
      return;
    }
    if (this.timeKeepingModel.code == "") {
      this.notificationsService.error("Cảnh báo", "Bạn chưa up nhập mã code");
      return;
    }
    let formData1 = new FormData();
    formData1.append("images", this.imageIcon);
    await this.timekeepingService.uploadFile(formData1).toPromise().then(res =>{
      if (res.Success) {
            console.log(44444, res.Message);
            this.anh = res.Message;
            this.timeKeepingModel.image = res.Message;
          }
    });

    //this.timeKeepingModel.image = this.anh;
    this.timeKeepingModel.latitude = this.latitude.toString();
    this.timeKeepingModel.longitude = this.longitude.toString();
    this.timeKeepingModel.workTime = new Date();

    this.timekeepingService
      .addTimeKeeping(this.timeKeepingModel)
      .subscribe((res) => {
        if (res.body.Data.Result == 1) {
          this.notificationsService.error(
            "Cảnh báo",
            "Không đúng mã code. Vui lòng thử lại"
          );
          return;
        }
        if (res.body.Data.Result == 2) {
          this.notificationsService.error(
            "Cảnh báo",
            "Đã quá giờ để chấm công"
          );
          return;
        }
        if (res.body.Data.Result == 4) {
          this.notificationsService.error(
            "Cảnh báo",
            "Bạn chưa được gửi mã code"
          );
          return;
        }
        if (!res.body.Success) {
          this.notificationsService.error("Cập nhật", res.Message);
          return;
        }
        this.notificationsService.success("Cập nhật", "Cập nhật thành công");
        this.backToList();
      });
  }

  async addCosts() {
    console.log(11, this.timeKeepingModel.listCost);
    if (this.timeKeepingModel.listCost.length == 0) {
      this.timeKeepingModel.listCost == null;
    }
    for (let i = 0; i < this.timeKeepingModel.listCost.length; i++) {
      let formData = new FormData();
      if (this.timeKeepingModel.listCost[i].imageFile == null) {
        this.timeKeepingModel.listCost == null;
        break;
      }
      formData.append("images", this.timeKeepingModel.listCost[i].imageFile);
      await this.timekeepingService
        .uploadFile(formData)
        .toPromise()
        .then((res) => {
          if (res.Success) {
            this.timeKeepingModel.listCost[i].imageUrl = res.Message;
          }
        });
    }
  }

  flag = 0;
  addFile() {
    this.flag++;
    let cost = {
      flag: this.flag,
      id: 0,
      currency: 0,
      title: "",
      note: "",
      createDate: new Date(),
      groupCostId: "",
      itemsCostId: "",
      timeKeepingId: 0,
      type: 1, //nhan vien
      imageUrl: "",
      imageFile: null,
    };
    // let formData = new FormData();
    // formData.append('images', cost.imageFile);
    // this.timekeepingService.uploadFile(formData).subscribe((res) => {
    //   if (res.Success) {
    //     cost.imageUrl = res.Message;
    //   }
    this.timeKeepingModel.listCost.push(cost);
    console.log(this.timeKeepingModel.listCost);
  }
  remove() {
    this.timeKeepingModel.listCost.splice(-1, 1);
  }
  backToList() {
    this.router.navigate(["/pages/timekeeping/list"]);
  }

  receiverTime(event) {
    this.timeKeepingModel.workTime = this.appService.covertTime(event);
  }

  onSelected(event) {
    this.taskService.getByUser(event).subscribe((result: any) => {
      this.lstTask = result.Data.Result;
      console.log(this.lstTask);
    });
  }
}

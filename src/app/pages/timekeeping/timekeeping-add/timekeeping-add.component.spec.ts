import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimekeepingAddComponent } from './timekeeping-add.component';

describe('TimekeepingAddComponent', () => {
  let component: TimekeepingAddComponent;
  let fixture: ComponentFixture<TimekeepingAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimekeepingAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimekeepingAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ProjectService } from './../../../common/services/project/project.service';
import { filter } from 'rxjs/operators';
import { User } from "./../../../@core/data/users";
import { Subscription } from "rxjs";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { ActionsColumnComponent } from "../../../shared/actions-column/actions-column.component";
import { ConfirmationComponent } from "../../../shared/confirmation/confirmation.component";
import { NbDialogService } from "@nebular/theme";
import { NavigationEnd, Router, ActivatedRoute } from "@angular/router";
import { NotificationsService } from "../../../common/services/notifications/notifications.service";
import { TimekeepingService } from "../../../common/services/timekeeping/timekeeping.service";
//import { AccountStatusComponent } from "../../account-management/account/account-status/account-status.component";
import { TimekeepingDetailComponent } from '../timekeeping-detail/timekeeping-detail.component';
import { AccountStatusComponent } from '../../account-management/account/account-status/account-status.component';
import { AccountService } from '../../../common/services/account/account.service';
import { TaskService } from '../../../common/services/task/task.service';
import { AppService } from '../../../common/services/AppService';

@Component({
  selector: "ngx-timekeeping-list",
  templateUrl: "./timekeeping-list.component.html",
  styleUrls: ["./timekeeping-list.component.scss"],
})
export class TimekeepingListComponent implements OnInit {
  idTask: number = 0;
  constructor(
    private routerActive: ActivatedRoute,
    private dialogService: NbDialogService,
    private router: Router,
    private projectService: ProjectService,
    private accountService: AccountService,
    private taskService: TaskService,
    private timekeepingService: TimekeepingService,
    private notificationsService: NotificationsService,
    private appService: AppService,
  ) {
    this.idTask = +this.routerActive.snapshot.paramMap.get("id");
  }

  searchModel = {
    projectId: 0,
    userId: "",
    taskId: 0,
    dateTime: "",
  };
lstProject = [];
lstTask = [];
lstUser = [];
public time: any = '';

  settings = {
    hideSubHeader: false, // hide filter row
    actions: false,
    columns: {
      WorkTime: {
        title: "Ngày",
        type: "string",
      },
      Project: {
        title: "Dự án",
        type: "string",
      },
      User: {
        title: "Nhân viên",
        type: "string",
      },

      Status: {
        title: "Trạng thái",
        type: "string",
      },
      actions: {
        title: "Thao Tác",
        type: "custom",
        width: "150px",
        renderComponent: ActionsColumnComponent,
        valuePrepareFunction: (cell, row) => {
          return row;
        },
        onComponentInitFunction: (instance) => {
          instance.edit.subscribe((row) => {
            this.gotoEditRow(row);
          });
          instance.delete.subscribe((row) => {
            this.gotoDeleteRow(row);
          });
        },
        editable: false,
        addable: false,
        filter: false,
      },
    },
  };
  data = [];

  ngOnInit() {
    this.getAll();
    this.getAllUser();
    this.getAllProject();
    this.getAllTask();
  }

  source: LocalDataSource = new LocalDataSource();

  getData() {
    this.source.load([]);
    this.timekeepingService.findAll(this.searchModel.userId, this.searchModel.projectId, this.searchModel.taskId, this.searchModel.dateTime).subscribe((result: any) => {
      this.source.load(result.Data);
    });
  }

  getAll() {
    this.source.load([]);
    this.timekeepingService.getAll().subscribe((result: any) => {
      if(this.idTask != 0)
      {
        this.source.load(result.Data.filter(x => x.TaskId == this.idTask));
      }
      else
      {
        this.source.load(result.Data);
      }

    });
  }

  gotoEditRow(row) {
    this.router.navigate([`/pages/timekeeping/edit/${row.Id}`]);
  }

  gotoDeleteRow(row) {
    this.dialogService
      .open(ConfirmationComponent, { context: { type: "Xác nhận xóa đơn chấm công" } })
      .onClose.subscribe((confirm) => {
        if (confirm) {
          this.timekeepingService.delete(row.Id).subscribe((res) => {
            if (!res.Success) {
              this.notificationsService.error("Xóa", res.Message);
              return;
            }
            if(res.Data == 1){
              this.notificationsService.error("Xóa", "Bạn không thể xóa chấm công do quản lý đã phê duyệt");
              return;
            }
            this.getAll();
            this.notificationsService.success(
              "Xóa",
              "Xóa chấm công thành công"
            );
          });
        }
      });
  }

  updateAddNew() {
    this.router.navigate(["/pages/timekeeping/add"]);
  }

  openWithBackdropClick(event) {
    this.open(event.data.Id);
  }

  protected open(id: number) {
    this.dialogService.open(TimekeepingDetailComponent, { context: {
      "idTimeKeeping":id,
    }, closeOnBackdropClick: true}).onClose.subscribe(s=>{

      this.getAll();

    },error=>{
      console.log(error)
    });
  }


  getAllUser() {
    this.accountService.getAll().subscribe((result: any) => {
      this.lstUser = result.Data;
    });
  }

  getAllTask() {
    this.taskService.getAll().subscribe((result: any) => {
      this.lstTask = result.Data;
      console.log(1111, this.lstTask);
    });
  }

  getAllProject() {
    this.projectService.getAll().subscribe((result: any) => {
      this.lstProject = result.Data;
    });
  }

  receiverEndTime(event){
    this.searchModel.dateTime = this.appService.covertTime(event);
    console.log(this.searchModel.dateTime);

  }

}

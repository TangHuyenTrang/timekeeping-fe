import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { Ng2SmartTableModule, LocalDataSource } from "ng2-smart-table";
import { NbDialogService, NbDialogRef } from "@nebular/theme";
import { NavigationEnd, Router } from "@angular/router";
import { NotificationsService } from "../../../common/services/notifications/notifications.service";
import { TimekeepingService } from '../../../common/services/timekeeping/timekeeping.service';
import { ConfirmationComponent } from '../../../shared/confirmation/confirmation.component';
import {TimekeepingListComponent} from '../timekeeping-list/timekeeping-list.component'
import { AngularEditorConfig } from "@kolkov/angular-editor";

@Component({
  selector: 'ngx-timekeeping-detail',
  templateUrl: './timekeeping-detail.component.html',
  styleUrls: ['./timekeeping-detail.component.scss']
})
export class TimekeepingDetailComponent implements OnInit {
  @Input() idTimeKeeping: number;
  config: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "15rem",
    minHeight: "5rem",
    placeholder: "Enter text here...",
    translate: "no",
    defaultParagraphSeparator: "p",
    defaultFontName: "Arial",
    toolbarHiddenButtons: [["bold"]],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: "redText",
        class: "redText",
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ],
  };
  constructor(
    private dialogService: NbDialogService,
    private router: Router,
    private timeKeepingService: TimekeepingService,
    private notificationsService: NotificationsService,
    protected ref: NbDialogRef<TimekeepingDetailComponent>,

  ) { }
  source: any = new LocalDataSource();
  ngOnInit() {
    this.getTimeKeepingDetail();

  }
  role = "";
  accept: boolean = true;
  timeKeepingModel = {
    shiftTime: '',
    workTime: '',
    description: '',
    user: '',
    image: '',
    document: '',
    project: '',
    task: '',
    status: '',
    latitude: 0,
    longitude: 0,
    reason:''
   };



  getAll() {
    this.source =[];
    this.timeKeepingService.getAll().subscribe((result: any) => {
      this.source = result.Data;
    });
  }
  getTimeKeepingDetail() {

    this.timeKeepingService
      .getTimeKeepingById(this.idTimeKeeping)
      .subscribe((result) => {
        this.role = result.Data.RoleId;
        this.timeKeepingModel.workTime = result.Data.WorkTime;
        this.timeKeepingModel.description = result.Data.Description;
        this.timeKeepingModel.project = result.Data.Project;
        this.timeKeepingModel.task = result.Data.TaskName;
        this.timeKeepingModel.user = result.Data.User;
        this.timeKeepingModel.status = result.Data.Status;
        this.timeKeepingModel.document = "http://localhost:1700/" + result.Data.Document;
        this.timeKeepingModel.image = "http://localhost:1700/" + result.Data.Image;
        this.timeKeepingModel.latitude = +result.Data.Latitude;
        this.timeKeepingModel.longitude = +result.Data.Longitude;
        this.timeKeepingModel.reason = result.Data.Reason;
        console.log(this.timeKeepingModel.description);

      });
      this.isNhanVien();
  }
  downloadFile(){
    window.open(this.timeKeepingModel.document);
  }

  chapNhanChamCong()
  {
    if(this.role === "52a2fa18-e9f9-407d-b18b-08d7cca1d864")
    {
      this.notificationsService.warn("Warning", "Bạn không đủ quyền");
    }
    else
    {

      this.timeKeepingService.accept(this.idTimeKeeping, 1, '').subscribe((res) => {
        if (!res.Success) {
          this.notificationsService.error("Cập nhật", res.Message);
          return;
        }
        this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      });
      this.dismiss();
    }

  }

  tuChoiChamCong()
  {
    console.log(22, this.timeKeepingModel);
    if(this.timeKeepingModel.status === "Success")
    {
      this.notificationsService.warn("Warning", "Bạn đã phê duyệt. Không thể từ chối");
      return;
    }
    if(this.role === "52a2fa18-e9f9-407d-b18b-08d7cca1d864")
    {
      this.notificationsService.warn("Warning", "Bạn không đủ quyền");
      return;
    }
    else
    {
      if(!this.timeKeepingModel.reason)
      {
        this.notificationsService.warn("Warning", "Bạn chưa nhập lý do từ chối");
        return;
      }
      this.timeKeepingService.accept(this.idTimeKeeping, 3, this.timeKeepingModel.reason).subscribe((res) => {
        if (!res.Success) {
          this.notificationsService.error("Cập nhật", res.Message);
          return;
        }
        this.notificationsService.success("Cập nhật", "Cập nhật thành công");
      });
      this.dismiss();
    }

  }
  dismiss() {
    this.ref.close(false);
  }
  backToList() {
    this.router.navigate(["/pages/timekeeping/list"]);
  }
  isNhanVien()
  {
    if(this.role == "52a2fa18-e9f9-407d-b18b-08d7cca1d864")
    {
      this.accept = false;
    }
  }
}

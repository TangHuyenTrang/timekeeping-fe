import { AuthService } from "./../../shared/service/auth.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "ngx-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  public dataLogin: any = {
    account: "",
    password: "",
  };
  constructor(private router: Router, private _authService: AuthService) {
    if (localStorage.getItem('token')) {
      this.router.navigate(['/']);
  }
  }

  ngOnInit() {}

  loginAccount() {
    if (this.dataLogin.account && this.dataLogin.password) {
      this._authService
        .login(this.dataLogin.account, this.dataLogin.password)
        .subscribe((res) => {
          if (res.Success) {
            console.log('Login thanhf cong',res);

            // Loggin thành công và lưu thông tin vào LocalStogate
            localStorage.setItem("token", JSON.stringify(res.Message));
            // Login thành công thì move sang màn pages
            this.router.navigate(["pages"]);
          }
          else
          {
            alert("Tên đăng nhập hoặc mật khẩu sai. Xin vui lòng thử lại");
          }
        });
    } else {
      alert("Vui lòng lòng đăng nhập");
    }
  }
  logOut() {
    //localStorage.removeItem("jwt");
    localStorage.removeItem("token");
  }
}

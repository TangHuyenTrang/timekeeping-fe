import { TestBed } from '@angular/core/testing';

import { ItemcostService } from './itemcost.service';

describe('ItemcostService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemcostService = TestBed.get(ItemcostService);
    expect(service).toBeTruthy();
  });
});

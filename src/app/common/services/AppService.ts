import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class AppService {
  public url: string = "https://localhost:44312/api/";
  public MenuItems: any = [

  ];
  constructor(private http: HttpClient) {

  }

  getListPerUser(): any {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage
        .getItem("token")
        .replace('"', "")
        .replace('"', "")}`,
    });
    let url = environment.domainAccount + "/User/GetListPerUser";
    return this.http
      .get(url, {
        headers: headers,
        //observe: 'response'
      })
      .map((resp) => {
        return resp;
      })
      .catch((err) => {
        return Observable.throw(err);
      });
  }


  // Lấy DL từ API
  getAPI(nameAPI: string = "", params: any = {}, debug: boolean = false) {
    const linkUrlNew = `${this.url}${nameAPI}?${this.objToQuery(params)}`;
    if (debug) {
      // dòng code check URL get API
      console.log(linkUrlNew);
    }
    return this.http.get(linkUrlNew);
  }
  // mã hóa một chuỗi uri
  objToQuery(obj) {
    let qrstr = Object.keys(obj)
      .reduce((a, k) => {
        a.push(k + "=" + encodeURIComponent(obj[k]));
        return a;
      }, [])
      .join("&");
    if (qrstr != "") {
      qrstr = "&" + qrstr;
    }
    return qrstr;
  }
  postAPI(nameAPI: string = "", dataPost: any = {}, debug: boolean = false) {
    const linkUrlNew = `${this.url}${nameAPI}`;
    if (debug) {
      // Check Data Post Lên API
      console.log(JSON.stringify(dataPost));
    }
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
    };
    return this.http.post(linkUrlNew, JSON.stringify(dataPost), httpOptions);
  }

  delete(nameAPI: string = "", params: any = {}, debug: boolean = false) {
    const linkUrl = `${this.url}${nameAPI}?${this.objToQuery(params)}`;
    if (debug) {
      // dòng code check URL get API
      console.log(linkUrl);
    }
    return this.http.delete(linkUrl);
  }
  put(nameAPI: string = "", dataPost: any = {}, debug: boolean = false) {
    const linkurl = `${this.url}${nameAPI}`;
    if (debug) {
      // Check Data Post Lên API
      console.log(JSON.stringify(dataPost));
    }
    const httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
    };
    return this.http.put(linkurl, JSON.stringify(dataPost), httpOptions);
  }

  // hàm chuyển time pick dạng new Date() sang year/month/day
  covertTime(event) {
    console.log(event);
    var year = event.getFullYear();
    var month = event.getMonth() + 1;
    var date = event.getDate();
    if (month < 10) {
      month = `0${month}`;
    }
    if (date < 10) {
      date = `0${date}`;
    }
    //  console.log(`${date}/${month}/${year}`);
    return `${month}/${date}/${year}`;
  }

  formatTimeDisplayNebular(time) {
    if (time && time != null) {
      const datatime = time.split("/");
      return new Date(
        Number(datatime[2]),
        Number(datatime[1]) - 1,
        Number(datatime[0])
      );
    }
  }
}

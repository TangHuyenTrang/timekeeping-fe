import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class CostService {

  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: `Bearer ${
      localStorage.getItem("token").replace('"','').replace('"','')
      }`
  });
  constructor( private http: HttpClient,
    private notificationsService: NotificationsService,) { }

    // getAll(type: number){
    //   let url = environment.domainAccount + '/Cost/GetAll';
    //   return this.http.get(url)
    //   .map(
    //     resp => {
    //       return resp;
    //     },
    //   )
    //     .catch(
    //       err => {
    //         this.notificationsService.error('Failed to fetch User',
    //           `Error: ${err.status} - ${err.statusText}`);
    //           return Observable.throw(err);
    //       },
    //     );
    // }

    getAll(type: number): any {
      return this.http.get(environment.domainAccount + '/Cost/GetAll?type=' + type)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    getByTimeKeeping(idTime: number): any {
      return this.http.get(environment.domainAccount + '/Cost/GetByTimeKeeping?idTime=' + idTime)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }


    updateCost(account: any): any {
      return this.http.put(environment.domainAccount + '/Cost/Update', account)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }




    getCostById(id: number): any {
      return this.http.get(environment.domainAccount + '/Cost/GetById?id=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
    accept(id: number, isAccept: number ): any {
      return this.http.get(environment.domainAccount + '/Cost/Accept?id=' + id + '&isAccept=' + isAccept,{
        headers: this.headers,
        observe: 'response'
      })
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
    add(account: any): any {
      return this.http.post(environment.domainAccount + '/Cost/Add', account)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }



    deleteCost(id: any): any {
      return this.http.delete(environment.domainAccount + '/Cost/Delete?id=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
}

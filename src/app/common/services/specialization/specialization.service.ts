import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

import 'rxjs/Rx';

@Injectable()
export class SpecializationService {

  constructor( private http: HttpClient,
    private notificationsService: NotificationsService,) { }

    getAll(){
      let url = environment.domainAccount + '/Specialization/GetAll';
      return this.http.get(url)
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    update(account: any): any {
      return this.http.put(environment.domainAccount + '/Specialization/Update', account)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }




    getById(id: string): any {
      return this.http.get(environment.domainAccount + '/Specialization/GetById?id=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    add(account: any): any {
      return this.http.post(environment.domainAccount + '/Specialization/Add', account)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }



    delete(id: any): any {
      return this.http.delete(environment.domainAccount + '/Specialization/Delete?id=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
}

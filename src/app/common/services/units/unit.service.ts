import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

//import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

//import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable()
export class UnitService {

  constructor(
    private http: HttpClient,
    private notificationsService: NotificationsService,
  ) { }

  getUnit(): any {
    return this.http.get(environment.domainAccount + '/Unit/GetAll')
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch Unit',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getUnitById(id): any {
    return this.http.get(environment.domainAccount + '/Unit/GetById?id=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch Unit',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getChildrenByIdParent(id): any {
    return this.http.get(environment.domainAccount + '/Unit/GetChildrenByParentId=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch Children Unit',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  addUnit(unit: any): any {
    console.log(unit)
    return this.http.post(environment.domainAccount + '/Unit/AddUnit', unit)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to add Unit',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  addUnitChildren(unitChildren: any): any {
    return this.http.post(environment.domainAccount + '/Unit/AddUnitChildren', unitChildren)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to add  Unit Children',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  editRole(unit: any): any {
    return this.http.put(environment.domainAccount + '/Unit/Update', unit)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to edit User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  deleteRole(id: any): any {
    return this.http.delete(environment.domainAccount + '/Unit/Delete?id=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to delete User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  uploadImage(image: File): any{
    let formData = new FormData();
    formData.append('images', image);
    return this.http.post(environment.domainAccount + '/Unit/Upload', formData)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to add Unit',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

import 'rxjs/Rx';

@Injectable()
export class GroupcostService {

  constructor( private http: HttpClient,
    private notificationsService: NotificationsService,) { }

    getAll(){
      let url = environment.domainAccount + '/GroupCost/GetAll';
      return this.http.get(url)
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    update(account: any): any {
      return this.http.put(environment.domainAccount + '/GroupCost/Update', account)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }




    getGroupCostById(id: string): any {
      return this.http.get(environment.domainAccount + '/GroupCost/GetById?id=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    add(account: any): any {
      return this.http.post(environment.domainAccount + '/GroupCost/Add', account)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }



    deleteGroupCost(id: any): any {
      return this.http.delete(environment.domainAccount + '/GroupCost/Delete?id=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
}

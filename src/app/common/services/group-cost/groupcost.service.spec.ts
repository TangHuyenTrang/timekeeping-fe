import { TestBed } from '@angular/core/testing';

import { GroupcostService } from './groupcost.service';

describe('GroupcostService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GroupcostService = TestBed.get(GroupcostService);
    expect(service).toBeTruthy();
  });
});

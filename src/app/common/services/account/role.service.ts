import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

//import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable()
export class RoleService {
  picture = 'assets/images/mainflux-logo.png';

  constructor(
    private http: HttpClient,
    private notificationsService: NotificationsService,
  ) { }

  getRole(): any {
    return this.http.get(environment.domainAccount + '/Role/GetAll')
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getRoleById(id): any {
    return this.http.get(environment.domainAccount + '/RolePermission/GetByIdRole?id=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  addRole(role: any): any {
    return this.http.post(environment.domainAccount + '/RolePermission/Add', role)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  editRole(role: any): any {
    return this.http.put(environment.domainAccount + '/RolePermission/Update', role)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  deleteRole(id: any): any {
    return this.http.delete(environment.domainAccount + '/RolePermission/Delete?id=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getAllPermission(): any {
    return this.http.get(environment.domainAccount + '/Permission/GetAll')
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }


  // changeUserPassword(passReq: any): any {
  //   return this.http.patch(environment.changePassUrl, passReq, { observe: 'response' })
  //     .map(
  //       resp => {
  //         return resp;
  //       },
  //     )
  //     .catch(
  //       err => {
  //         this.notificationsService.error('Failed to change User password',
  //           `Error: ${err.status} - ${err.statusText}`);
  //           return Observable.throw(err);
  //       },
  //     );
  // }
}

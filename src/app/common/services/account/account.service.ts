import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

//import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable()
export class AccountService {
  picture = 'assets/images/mainflux-logo.png';

  constructor(
    private http: HttpClient,
    private notificationsService: NotificationsService,
  ) { }

  getAll(){
    let url = environment.domainAccount + '/User/GetAll';
    return this.http.get(url)
    .map(
      resp => {
        return resp;
      },
    )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  addUpdate(account: any): any {
    return this.http.put(environment.domainAccount + '/User/Update', account)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getUnits(){
    let url = environment.domainDevice + '/GetAllForDropDown';
    return this.http.get(url)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getSpecials(): any{
    let url = environment.domainAccount + '/Specialization/GetAll';
    return this.http.get(url)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getListPerUser(): any{
    let url = environment.domainAccount + '/User/GetListPerUser';
    return this.http.get(url)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getAccountById(id: string): any {
    return this.http.get(environment.domainAccount + '/User/GetById?id=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  addAccount(account: any): any {
    return this.http.post(environment.domainAccount + '/User/Add', account)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }



  deleteAccount(id: any): any {
    return this.http.delete(environment.domainAccount + '/User/Delete?id=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }



}

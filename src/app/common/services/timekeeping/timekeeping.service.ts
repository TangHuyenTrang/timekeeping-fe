import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

//import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class TimekeepingService {

  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: `Bearer ${
      localStorage.getItem("token").replace('"','').replace('"','')
      }`
  });


  constructor(
    private http: HttpClient,
    private notificationsService: NotificationsService,
  ) {

   }

  // get address



  //
  getAll(){
    let url = environment.domainAccount + '/TimeKeeping/GetAll';
    return this.http.get(url, {
      headers: this.headers
              //observe: 'response'
    })
    .map(
      resp => {
        return resp;
      },
    )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch TimeKeeping',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  findAll(userId: string, projectId: number, taskId: number, dateTime: any){
    let url = environment.domainAccount + '/TimeKeeping/FindAll?userId=' +userId +  '&projectId='+ projectId + '&taskId=' + taskId +  '&dateTime=' + dateTime;
    return this.http.get(url)
    .map(
      resp => {
        return resp;
      },
    )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch TimeKeeping',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  getAllBySuccess(){
    let url = environment.domainAccount + '/TimeKeeping/GetAllBySuccess';
    return this.http.get(url)
    .map(
      resp => {
        return resp;
      },
    )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch TimeKeeping',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  updateTimeKeeping(id: number, fileUrl: string): any {
    return this.http.get(environment.domainAccount + '/TimeKeeping/Update?id='+ id + '&fileUrl=' + fileUrl)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch TimeKeeping',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }


  getTimeKeepingById(id: number): any {
    return this.http.get(environment.domainAccount + '/TimeKeeping/GetById?id=' + id, {
      headers: this.headers
              //observe: 'response'
    })
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  accept(id: number, isAccept: number, reason: string): any {
    return this.http.get(environment.domainAccount + '/TimeKeeping/Accept?id=' + id + '&isAccept=' + isAccept + '&reason=' + reason)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  addTimeKeeping(timeKeeping: any): any {
    return this.http.post(environment.domainAccount + '/TimeKeeping/Add', timeKeeping ,{
      headers: this.headers,
      observe: 'response'
    })
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }



  delete(id: any): any {
    return this.http.delete(environment.domainAccount + '/TimeKeeping/Delete?id=' + id)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch TimeKeeping',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }


  uploadFile(formData: FormData): any {
    return this.http.post(environment.domainAccount + '/TimeKeeping/UploadFile', formData)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  uploadDocument(formData: FormData): any {
    return this.http.post(environment.domainAccount + '/TimeKeeping/UploadDocument', formData)
      .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch User',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
  }

  // tinhLuong(startDate: any, endDate: any): any {
  //   return this.http.get(environment.domainAccount + '/TimeKeeping/TinhLuong?startDate=' + startDate + '&endDate=' + endDate)
  //     .map(
  //       resp => {
  //         return resp;
  //       },
  //     )
  //     .catch(
  //       err => {
  //         this.notificationsService.error('Failed to fetch User',
  //           `Error: ${err.status} - ${err.statusText}`);
  //           return Observable.throw(err);
  //       },
  //     );
  // }
  tinhLuong(startDate: any, endDate: any) {
    const queryString = environment.domainAccount + '/TimeKeeping/TinhLuong?startDate=' + startDate + '&endDate=' + endDate;
    return this.http.get(queryString, { responseType: 'arraybuffer' }).toPromise();
  }

  tinhChiPhi(startDate: any, endDate: any) {
    const queryString = environment.domainAccount + '/TimeKeeping/TinhChiPhi?startDate=' + startDate + '&endDate=' + endDate;
    return this.http.get(queryString, { responseType: 'arraybuffer' }).toPromise();
  }


}

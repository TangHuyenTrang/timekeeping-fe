import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

import 'rxjs/Rx';
@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient,
    private notificationsService: NotificationsService,) { }
    //headers : any;
    headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${
        localStorage.getItem("token").replace('"','').replace('"','')
        }`
    });

    getAll() {
      let url = environment.domainAccount + '/Project/GetAll';
      return this.http.get(url, {
        headers: this.headers
                //observe: 'response'
      }) .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch Project',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
    }

    getAllByUser() {
      let url = environment.domainAccount + '/Project/GetAllByUser';
      return this.http.get(url, {
        headers: this.headers
                //observe: 'response'
      }) .map(
        resp => {
          return resp;
        },
      )
      .catch(
        err => {
          this.notificationsService.error('Failed to fetch Project',
            `Error: ${err.status} - ${err.statusText}`);
            return Observable.throw(err);
        },
      );
    }
    // getAll(){
    //   let url = environment.domainAccount + '/Project/GetAll';
    //   return this.http.get(url)
    //   .map(
    //     resp => {
    //       return resp;
    //     },
    //   )
    //     .catch(
    //       err => {
    //         this.notificationsService.error('Failed to fetch Project',
    //           `Error: ${err.status} - ${err.statusText}`);
    //           return Observable.throw(err);
    //       },
    //     );
    // }

    updateProject(project: any): any {
      let url = environment.domainAccount + '/Project/Update';
      return this.http.put(url,  project,{
        headers: this.headers,
        observe: 'response'
      })
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Project',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }



    getProjectById(id: number): any {
      return this.http.get(environment.domainAccount + '/Project/GetById?id=' + id, {
        headers: this.headers,
        observe: 'response'
      })
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Project',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    addProject(project: any): any {
      return this.http.post(environment.domainAccount + '/Project/Add', project, {
        headers: this.headers,
        observe: 'response'
      })
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Project',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }



    deleteProject(id: number): any {
      return this.http.delete(environment.domainAccount + '/Project/Delete?id=' + id, {
        headers: this.headers,
        observe: 'response'
      })
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Project',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
}

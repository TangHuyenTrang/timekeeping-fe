import { TestBed } from '@angular/core/testing';

import { PublicHolidayService } from './public-holiday.service';

describe('PublicHolidayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PublicHolidayService = TestBed.get(PublicHolidayService);
    expect(service).toBeTruthy();
  });
});

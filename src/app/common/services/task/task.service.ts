import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, interval } from 'rxjs';

import { environment } from '../../../environments/environment';
import { NotificationsService } from '../notifications/notifications.service';

import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  headers = new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: `Bearer ${
      localStorage.getItem("token").replace('"','').replace('"','')
      }`
  });

  constructor( private http: HttpClient,
    private notificationsService: NotificationsService,) { }

    getAll(){
      let url = environment.domainAccount + '/Task/GetAll';
      return this.http.get(url, {
        headers: this.headers
      })
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    getProjectByUser(){
      let url = environment.domainAccount + '/Task/GetProjectByUser';
      return this.http.get(url, {
        headers: this.headers
                //observe: 'response'
      })
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Task',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    getNoti(dateNow: any){
      let url = environment.domainAccount + '/Task/GetNoti?dateNow=' + dateNow;
      return this.http.get(url, {
        headers: this.headers
                //observe: 'response'
      })
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Task',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    getByUser(id: number){
      let url = environment.domainAccount + '/Task/GetByUser?projectId=' + id;
      return this.http.get(url, {
        headers: this.headers
      })
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Task',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    getById(id: number){
      let url = environment.domainAccount + '/Task/GetById?Id=' + id;
      return this.http.get(url)
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Task',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
    getHistory(taskId: number){
      let url = environment.domainAccount + '/Task/GetHistoryTask?taskId=' + taskId;
      return this.http.get(url)
      .map(
        resp => {
          return resp;
        },
      )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Task',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    update(task: any): any {
      console.log(this.headers);

      let url = environment.domainAccount + '/Task/Update';
      return this.http.put(url, task,{
        headers: this.headers,
        observe: 'response'
      })
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch Project',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }




    getTaskById(id: number): any {
      return this.http.get(environment.domainAccount + '/Task/GetByIdProject?idProject=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    add(account: any): any {
      return this.http.post(environment.domainAccount + '/Task/Add', account)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }

    delete(id: NumberConstructor): any {
      return this.http.delete(environment.domainAccount + '/Task/Delete?id=' + id)
        .map(
          resp => {
            return resp;
          },
        )
        .catch(
          err => {
            this.notificationsService.error('Failed to fetch User',
              `Error: ${err.status} - ${err.statusText}`);
              return Observable.throw(err);
          },
        );
    }
}

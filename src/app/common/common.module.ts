//import { BrowserModule } from '@angular/platform-browser';
import { UnitService } from './services/units/unit.service';
import { NgModule } from '@angular/core';

import { RoleService } from './services/account/role.service';
import { NotificationsService } from './services/notifications/notifications.service';
//import { HTTP_INTERCEPTORS } from '@angular/common/http';
@NgModule({
  //imports:[BrowserModule],
  providers: [
    RoleService,
    NotificationsService,
    UnitService
  ],
})
export class CommonModule { }

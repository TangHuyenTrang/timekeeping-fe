/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { CoreModule } from "./@core/core.module";
import { ThemeModule } from "./@theme/theme.module";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";

import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
  NbLayoutModule,
  NbDialogService,
} from "@nebular/theme";
import { DashboardModule } from "./pages/dashboard/dashboard.module";
import { ECommerceModule } from "./pages/e-commerce/e-commerce.module";
import { MiscellaneousModule } from "./pages/miscellaneous/miscellaneous.module";
import { SharedModule } from "./shared/shared.module";
import { LoginComponent } from "./auth/login/login.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NotificationsService } from './common/services/notifications/notifications.service';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AppComponent, LoginComponent],
  imports: [

    BrowserModule,
    CommonModule ,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    SharedModule,
    NbLayoutModule,
    ReactiveFormsModule,
    FormsModule,

    ThemeModule.forRoot(),

    NbDialogModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: "AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY",
    }),
    CoreModule.forRoot(),
  ],
  providers: [
    NotificationsService
    // NbDialogService
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
